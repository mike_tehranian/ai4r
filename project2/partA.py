#
# === Introduction ===
#
# In this problem, you will build a planner that helps a robot
#   find the best path through a warehouse filled with boxes
#   that it has to pick up and deliver to a dropzone.
#
# Your file must be called `partA.py` and must have a class
#   called `DeliveryPlanner`.
# This class must have an `__init__` function that takes three
#   arguments: `self`, `warehouse`, and `todo`.
# The class must also have a function called `plan_delivery` that
#   takes a single argument, `self`.
#
# === Input Specifications ===
#
# `warehouse` will be a list of m strings, each with n characters,
#   corresponding to the layout of the warehouse. The warehouse is an
#   m x n grid. warehouse[i][j] corresponds to the spot in the ith row
#   and jth column of the warehouse, where the 0th row is the northern
#   end of the warehouse and the 0th column is the western end.
#
# The characters in each string will be one of the following:
#
# '.' (period) : traversable space. The robot may enter from any adjacent space.
# '#' (hash) : a wall. The robot cannot enter this space.
# '@' (dropzone): the starting point for the robot and the space where all boxes must be delivered.
#   The dropzone may be traversed like a '.' space.
# [0-9a-zA-Z] (any alphanumeric character) : a box. At most one of each alphanumeric character
#   will be present in the warehouse (meaning there will be at most 62 boxes). A box may not
#   be traversed, but if the robot is adjacent to the box, the robot can pick up the box.
#   Once the box has been removed, the space functions as a '.' space.
#
# For example,
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   is a 3x3 warehouse.
#   - The dropzone is at the warehouse cell in row 2, column 2.
#   - Box '1' is located in the warehouse cell in row 0, column 0.
#   - Box '2' is located in the warehouse cell in row 0, column 2.
#   - There are walls in the warehouse cells in row 0, column 1 and row 1, column 1.
#   - The remaining five warehouse cells contain empty space.
#
# The argument `todo` is a list of alphanumeric characters giving the order in which the
#   boxes must be delivered to the dropzone. For example, if
#   todo = ['1','2']
#   is given with the above example `warehouse`, then the robot must first deliver box '1'
#   to the dropzone, and then the robot must deliver box '2' to the dropzone.
#
# === Rules for Movement ===
#
# - Two spaces are considered adjacent if they share an edge or a corner.
# - The robot may move horizontally or vertically at a cost of 2 per move.
# - The robot may move diagonally at a cost of 3 per move.
# - The robot may not move outside the warehouse.
# - The warehouse does not "wrap" around.
# - As described earlier, the robot may pick up a box that is in an adjacent square.
# - The cost to pick up a box is 4, regardless of the direction the box is relative to the robot.
# - While holding a box, the robot may not pick up another box.
# - The robot may put a box down on an adjacent empty space ('.') or the dropzone ('@') at a cost
#   of 2 (regardless of the direction in which the robot puts down the box).
# - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
#   house.
# - The warehouse will be arranged so that it is always possible for the robot to move to the
#   next box on the todo list without having to rearrange any other boxes.
#
# An illegal move will incur a cost of 100, and the robot will not move (the standard costs for a
#   move will not be additionally incurred). Illegal moves include:
# - attempting to move to a nonadjacent, nonexistent, or occupied space
# - attempting to pick up a nonadjacent or nonexistent box
# - attempting to pick up a box while holding one already
# - attempting to put down a box on a nonadjacent, nonexistent, or occupied space
# - attempting to put down a box while not holding one
#
# === Output Specifications ===
#
# `plan_delivery` should return a LIST of moves that minimizes the total cost of completing
#   the task successfully.
# Each move should be a string formatted as follows:
#
# 'move {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot moves
#   to and '{j}' is replaced by the column-coordinate of the space the robot moves to
#
# 'lift {x}', where '{x}' is replaced by the alphanumeric character of the box being picked up
#
# 'down {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot puts
#   the box, and '{j}' is replaced by the column-coordinate of the space the robot puts the box
#
# For example, for the values of `warehouse` and `todo` given previously (reproduced below):
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   todo = ['1','2']
# `plan_delivery` might return the following:
#   ['move 2 1',
#    'move 1 0',
#    'lift 1',
#    'move 2 1',
#    'down 2 2',
#    'move 1 2',
#    'lift 2',
#    'down 2 2']
#
# === Grading ===
#
# - Your planner will be graded against a set of test cases, each equally weighted.
# - If your planner returns a list of moves of total cost that is K times the minimum cost of
#   successfully completing the task, you will receive 1/K of the credit for that test case.
# - Otherwise, you will receive no credit for that test case. This could happen for one of several
#   reasons including (but not necessarily limited to):
#   - plan_delivery's moves do not deliver the boxes in the correct order.
#   - plan_delivery's output is not a list of strings in the prescribed format.
#   - plan_delivery does not return an output within the prescribed time limit.
#   - Your code raises an exception.
#
# === Additional Info ===
#
# - You may add additional classes and functions as needed provided they are all in the file `partA.py`.
# - Upload partA.py to Project 2 on T-Square in the Assignments section. Do not put it into an
#   archive with other files.
# - Ask any questions about the directions or specifications on Piazza.
#

import heapq
import robot

class DeliveryPlanner:

    def __init__(self, warehouse, todo):
        # Make sure to deep copy the warehouse
        self.warehouse = list(warehouse)
        self.todo = todo
        self.rows = len(self.warehouse)
        self.cols = len(self.warehouse[0])
        self.dropzone = self.find_dropzone(warehouse)

    def plan_delivery(self):
        # moves = ['move 2 1',
                 # 'move 1 0',
                 # 'lift 1',
                 # 'move 2 1',
                 # 'down 2 2',
                 # 'move 1 2',
                 # 'lift 2',
                 # 'down 2 2']

        moves = []
        current = self.dropzone

        for goal_id in self.todo:
            goal = self.find_goal_id_on_map(goal_id)
            path = self.a_star(current, goal)
            # Don't need the start
            if len(path) > 1:
                path.pop(0)
            for move in path:
                z = "move {} {}".format(move[0], move[1])
                moves.append(z)
            z = "lift {}".format(goal_id)
            moves.append(z)

            # Update the current location
            if len(path) > 0:
                current = path[-1]

            # Remove goal object from the warehouse map
            row_as_list = list(self.warehouse[goal[0]])
            row_as_list[goal[1]] = "."
            self.warehouse[goal[0]] = "".join(row_as_list)

            # Reverse the path back to the dropzone
            path = self.a_star(current, self.dropzone)
            # Don't need the start
            if len(path) > 1:
                path.pop(0)
            for move in path:
                z = "move {} {}".format(move[0], move[1])
                moves.append(z)
            z = "down {} {}".format(self.dropzone[0], self.dropzone[1])
            moves.append(z)

            # Update the current location
            if len(path) > 0:
                current = path[-1]

        return moves

    def find_dropzone(self, warehouse):
        # Find the dropzone and save it
        for row in range(self.rows):
            for col in range(self.cols):
                if warehouse[row][col] == "@":
                    return (row, col)

    def is_adjacent_to_dropzone(self, p):
        return (self.is_adjacent(p, self.dropdzone))

    def get_adjacent_neighbors(self, p):
        delta = [[-1, 0], # go up
                 [ 0,-1], # go left
                 [ 1, 0], # go down
                 [ 0, 1], # go right
                 [-1, -1], # go up-left
                 [-1, 1], # go up-right
                 [1, -1], # go down-left
                 [1, 1]] # go down-right

        x, y = p
        neighbors = []

        for i in range(len(delta)):
            x2 = x + delta[i][0]
            y2 = y + delta[i][1]
            if x2 >= 0 and x2 < self.rows and y2 >= 0 and y2 < self.cols:
                cell_value = self.warehouse[x2][y2]
                if cell_value == '@' or cell_value == '.':
                    # first four moves cost 2, last four cost 3
                    cost = 2 if i <= 3 else 3
                    neighbors.append(((x2, y2), cost))

        return neighbors

    def is_goal_adjacent(self, goal, p):
        delta = [[-1, 0], # go up
                 [ 0,-1], # go left
                 [ 1, 0], # go down
                 [ 0, 1], # go right
                 [-1, -1], # go up-left
                 [-1, 1], # go up-right
                 [1, -1], # go down-left
                 [1, 1]] # go down-right

        x, y = p

        for i in range(len(delta)):
            x2 = x + delta[i][0]
            y2 = y + delta[i][1]

            if (x2, y2) == goal:
                return True

        return False

    def find_goal_id_on_map(self, goal_id):
        for row in range(self.rows):
            for col in range(self.cols):
                if self.warehouse[row][col] == goal_id:
                    return (row, col)

        # raise ValueError("Invalid Goal ID: Does not exist on the map")

    def a_star(self, start, goal):
        heuristic = robot.compute_distance

        if start == goal:
            # Find an empty adjacent space and go there
            open_spaces = self.get_adjacent_neighbors(start)
            if len(open_spaces) > 0:
                return [open_spaces[0][0]]
            # raise ValueError("Bug in code: No free adjacent spaces")

        if self.is_goal_adjacent(goal, start):
            return []

        goal_x, goal_y = goal

        frontier = PriorityQueue()
        frontier.append((heuristic(start, goal), start))
        explored = set()

        parent_node = {}
        g_cost_to_node = {}
        g_cost_to_node[start] = 0

        found_goal = False
        last_node_visited = None

        while frontier.size() > 0:
            _, current_node = frontier.pop()

            if self.is_goal_adjacent(goal, current_node):
                last_node_visited = current_node
                found_goal = True
                break

            current_neighbors = self.get_adjacent_neighbors(current_node)

            explored.add(current_node)
            for neighbor, cost in current_neighbors:
                if neighbor not in explored and neighbor not in frontier:
                    # First time seeing the node
                    new_g_cost = g_cost_to_node[current_node] + cost
                    g_cost_to_node[neighbor] = new_g_cost
                    new_h_cost = heuristic(neighbor, goal)

                    frontier.append((new_g_cost + new_h_cost, neighbor))
                    parent_node[neighbor] = current_node
                elif neighbor in frontier: # Dunder contains is overridden so this works
                    # h costs cancel out so they can be ignored when comparing
                    # the incumbent and new f costs
                    incumbent_cost = g_cost_to_node[neighbor]
                    new_child_cost = g_cost_to_node[current_node] + cost
                    if new_child_cost < incumbent_cost:
                        frontier.remove(neighbor) # Remove by node ID lookup
                        g_cost_to_node[neighbor] = new_child_cost
                        frontier.append((new_child_cost + heuristic(neighbor, goal), neighbor))
                        # Overwrite a new parent node for the current node
                        parent_node[neighbor] = current_node

        # if not found_goal:
           # raise ValueError("Did not find goal in the map")

        previous_node = last_node_visited
        path_to_goal = [previous_node]
        while previous_node != start:
            previous_node = parent_node[previous_node]
            path_to_goal = [previous_node] + path_to_goal

        return path_to_goal


def is_adjacent(p, q):
    x1, y1 = p
    x2, y2 = q

    adjacent = abs(x2-x1) <= 1 and abs(y2-y1) <= 1
    return adjacent


# This PriorityQueue class implementation is based upon work I originally
# completed and submitted as part of project 2 in CS6601 - Aritificial Intelligence.
# It is a re-usable class and is needed to make up for a lack of a robust
# PriorityQueue implementation in the Python standard library.
class PriorityQueue():

    def __init__(self):
        self.queue = []
        self.current = 0

    def next(self):
        if self.current >=len(self.queue):
            self.current
            raise StopIteration

        out = self.queue[self.current]
        self.current += 1

        return out

    def pop(self):
        top_of_heap = heapq.heappop(self.queue)
        return top_of_heap

    def remove(self, nodeId):
        for i in range(self.size()):
            if self.queue[i][1] == nodeId:
                del self.queue[i]
                heapq.heapify(self.queue)
                break

    def __iter__(self):
        return self

    def __str__(self):
        return 'PQ:[%s]'%(', '.join([str(i) for i in self.queue]))

    def append(self, node):
        heapq.heappush(self.queue, node)

    def __contains__(self, key):
        self.current = 0
        return key in [n for v,n in self.queue]

    def __eq__(self, other):
        self.current = 0
        return self == other

    def size(self):
        return len(self.queue)

    def clear(self):
        self.queue = []

    def top(self):
        return self.queue[0]

    __next__ = next

