# This file is your main submission that will be graded against. Only copy-paste
# code on the relevant classes included here from the IPython notebook. Do not
# add any classes or functions to this file that are not part of the classes
# that we want.
from __future__ import division
import math
from osm2networkx import *
import random
import pickle
import sys
import os
# Comment the next line when submitting to bonnie
# import matplotlib.pyplot as plt

# Implement a heapq backed priority queue (accompanying the relevant question)
import heapq

class PriorityQueue():

    def __init__(self):
        self.queue = []
        self.current = 0

    def next(self):
        if self.current >=len(self.queue):
            self.current
            raise StopIteration

        out = self.queue[self.current]
        self.current += 1

        return out

    def pop(self):
        top_of_heap = heapq.heappop(self.queue)
        return top_of_heap

    def remove(self, nodeId):
        for i in range(self.size()):
            if self.queue[i][1] == nodeId:
                del self.queue[i]
                heapq.heapify(self.queue)
                break

    def __iter__(self):
        return self

    def __str__(self):
        return 'PQ:[%s]'%(', '.join([str(i) for i in self.queue]))

    def append(self, node):
        heapq.heappush(self.queue, node)

    def __contains__(self, key):
        self.current = 0
        return key in [n for v,n in self.queue]

    def __eq__(self, other):
        self.current = 0
        return self == other

    def size(self):
        return len(self.queue)

    def clear(self):
        self.queue = []

    def top(self):
        return self.queue[0]

    __next__ = next

#Warmup exercise: Implement breadth-first-search
def breadth_first_search(graph, start, goal):
    if start == goal:
        return []

    parent_node = {}
    frontier = [start]
    explored = set()

    while frontier:
        current_node = frontier.pop(0)
        explored.add(current_node)
        for neighbor in graph.neighbors(current_node):
            if neighbor not in explored and neighbor not in frontier:
                parent_node[neighbor] = current_node
                if neighbor == goal:
                    # Contruct the path back to the start node
                    previous_node = goal
                    path_to_goal = [previous_node]
                    while previous_node != start:
                        previous_node = parent_node[previous_node]
                        path_to_goal = [previous_node] + path_to_goal
                    return path_to_goal
                frontier.append(neighbor)

#Warmup exercise: Implement uniform_cost_search
def uniform_cost_search(graph, start, goal):
    if start == goal:
        return []

    frontier = PriorityQueue()
    frontier.append((0, start))
    explored = set()

    parent_node = {}
    cost_to_node = {}
    cost_to_node[start] = 0

    while frontier:
        current_node_cost, current_node = frontier.pop()
        if current_node == goal:
            break

        explored.add(current_node)
        for neighbor in graph.neighbors(current_node):
            # if neighbor not in explored:
            if neighbor not in explored and neighbor not in frontier:
                # First time seeing the node
                new_child_cost = current_node_cost + graph.edge[current_node][neighbor]['weight']
                cost_to_node[neighbor] = new_child_cost
                frontier.append((new_child_cost, neighbor))
                parent_node[neighbor] = current_node
            elif neighbor in frontier: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node[neighbor]
                new_child_cost = current_node_cost + graph.edge[current_node][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier.remove(neighbor) # Remove by node ID lookup
                    cost_to_node[neighbor] = new_child_cost
                    frontier.append((new_child_cost, neighbor))
                    # Overwrite a new parent node for the current node
                    parent_node[neighbor] = current_node

    previous_node = goal
    path_to_goal = [previous_node]
    while previous_node != start:
        previous_node = parent_node[previous_node]
        path_to_goal = [previous_node] + path_to_goal

    return path_to_goal

# Warmup exercise: Implement A*
def null_heuristic(graph, v, goal ):
    return 0

# Warmup exercise: Implement the euclidean distance heuristic
def euclidean_dist_heuristic(graph, v, goal):
    if 'pos' in graph.node[v]:
        current_position = graph.node[v]['pos']
        goal_position = graph.node[goal]['pos']
    else:
        current_position = graph.node[v]['position']
        goal_position = graph.node[goal]['position']

    return math.sqrt((current_position[0] - goal_position[0])**2 + (current_position[1] - goal_position[1])**2)

# Warmup exercise: Implement A* algorithm
def a_star(graph, start, goal, heuristic=euclidean_dist_heuristic):
    if start == goal:
        return []

    if goal in graph.neighbors(start):
        return [start, goal]

    frontier = PriorityQueue()
    frontier.append((heuristic(graph, start, goal), start))
    explored = set()

    parent_node = {}
    g_cost_to_node = {}
    g_cost_to_node[start] = 0

    while frontier:
        _, current_node = frontier.pop()
        if current_node == goal:
            break

        explored.add(current_node)
        for neighbor in graph.neighbors(current_node):
            if neighbor not in explored and neighbor not in frontier:
                # First time seeing the node
                new_g_cost = g_cost_to_node[current_node] + graph.edge[current_node][neighbor]['weight']
                g_cost_to_node[neighbor] = new_g_cost
                new_h_cost = heuristic(graph, neighbor, goal)

                frontier.append((new_g_cost + new_h_cost, neighbor))
                parent_node[neighbor] = current_node
            elif neighbor in frontier: # Dunder contains is overridden so this works
                # h costs cancel out so they can be ignored when comparing the incumbent and new f costs
                incumbent_cost = g_cost_to_node[neighbor]
                new_child_cost = g_cost_to_node[current_node] + graph.edge[current_node][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier.remove(neighbor) # Remove by node ID lookup
                    g_cost_to_node[neighbor] = new_child_cost
                    frontier.append((new_child_cost + heuristic(graph, neighbor, goal), neighbor))
                    # Overwrite a new parent node for the current node
                    parent_node[neighbor] = current_node

    previous_node = goal
    path_to_goal = [previous_node]
    while previous_node != start:
        previous_node = parent_node[previous_node]
        path_to_goal = [previous_node] + path_to_goal

    return path_to_goal

