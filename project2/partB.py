#
# === Introduction ===
#
# In this problem, you will again build a planner that helps a robot
#   find the best path through a warehouse filled with boxes
#   that it has to pick up and deliver to a dropzone. Unlike Part A,
#   however, in this problem the robot is moving in a continuous world
#   (albeit in discrete time steps) and has constraints on the amount
#   it can turn its wheels in a given time step.
#
# Your file must be called `partB.py` and must have a class
#   called `DeliveryPlanner`.
# This class must have an `__init__` function that takes five
#   arguments: `self`, `warehouse`, `todo`, `max_distance`, and
#   `max_steering`.
# The class must also have a function called `plan_delivery` that
#   takes a single argument, `self`.
#
# === Input Specifications ===
#
# `warehouse` will be a list of m strings, each with n characters,
#   corresponding to the layout of the warehouse. The warehouse is an
#   m x n grid. warehouse[i][j] corresponds to the spot in the ith row
#   and jth column of the warehouse, where the 0th row is the northern
#   end of the warehouse and the 0th column is the western end.
#
# The characters in each string will be one of the following:
#
# '.' (period) : traversable space.
# '#' (hash) : a wall. If the robot contacts a wall space, it will crash.
# '@' (dropzone): the space where all boxes must be delivered. The dropzone may be traversed like
#   a '.' space.
#
# Each space is a 1 x 1 block. The upper-left corner of space warehouse[i][j] is at the point (j,-i) in
#   the plane. Spaces outside the warehouse are considered walls; if any part of the robot leaves the
#   warehouse, it will be considered to have crashed into the exterior wall of the warehouse.
#
# For example,
#   warehouse = ['.#.',
#                '.#.',
#                '..@']
#   is a 3x3 warehouse. The dropzone is at space (2,2) and there are walls at spaces (0,1)
#   and (1,1). The rest of the warehouse is empty space.
#
# The robot is a circle of radius 0.25. The robot begins centered in the dropzone space.
#   The robot's initial bearing is 0.
#
# The argument `todo` is a list of points representing the center point of each box.
#   todo[0] is the first box which must be delivered, followed by todo[1], and so on.
#   Each box is a square of size 0.2 x 0.2. If the robot contacts a box, it will crash.
#
# The arguments `max_distance` and `max_steering` are parameters constraining the movement
#   of the robot on a given time step. They are described more below.
#
# === Rules for Movement ===
#
# - The robot may move any distance between 0 and `max_distance` per time step.
# - The robot may set its steering angle anywhere between -`max_steering` and
#   `max_steering` per time step. A steering angle of 0 means that the robot will
#   move according to its current bearing. A positive angle means the robot will
#   turn counterclockwise by `steering_angle` radians; a negative steering_angle
#   means the robot will turn clockwise by abs(steering_angle) radians.
# - Upon a movement, the robot will change its steering angle instantaneously to the
#   amount indicated by the move, and then it will move a distance in a straight line in its
#   new bearing according to the amount indicated move.
# - The cost per turn is 1 plus the amount of distance traversed by the robot on that turn.
#
# - The robot may pick up a box whose center point is within 0.5 units of the robot's center point.
# - If the robot picks up a box, it incurs a total cost of 2 for that turn (this already includes
#   the 1-per-turn cost incurred by the robot).
# - While holding a box, the robot may not pick up another box.
# - The robot may put a box down at a total cost of 1.5 for that turn. The box must be placed so that:
#   - The box is not contacting any walls, the exterior of the warehouse, any other boxes, or the robot
#   - The box's center point is within 0.5 units of the robot's center point
# - A box is always oriented so that two of its edges are horizontal and the other two are vertical.
# - If a box is placed entirely within the '@' space, it is considered delivered and is removed from the
#   warehouse.
# - The warehouse will be arranged so that it is always possible for the robot to move to the
#   next box on the todo list without having to rearrange any other boxes.
#
# - If the robot crashes, it will stop moving and incur a cost of 100*distance, where distance
#   is the length it attempted to move that turn. (The regular movement cost will not apply.)
# - If an illegal move is attempted, the robot will not move, but the standard cost will be incurred.
#   Illegal moves include (but are not necessarily limited to):
#     - picking up a box that doesn't exist or is too far away
#     - picking up a box while already holding one
#     - putting down a box too far away or so that it's touching a wall, the warehouse exterior,
#       another box, or the robot
#     - putting down a box while not holding a box
#
# === Output Specifications ===
#
# `plan_delivery` should return a LIST of strings, each in one of the following formats.
#
# 'move {steering} {distance}', where '{steering}' is a floating-point number between
#   -`max_steering` and `max_steering` (inclusive) and '{distance}' is a floating-point
#   number between 0 and `max_distance`
#
# 'lift {b}', where '{b}' is replaced by the index in the list `todo` of the box being picked up
#   (so if you intend to lift box 0, you would return the string 'lift 0')
#
# 'down {x} {y}', where '{x}' is replaced by the x-coordinate of the center point of where the box
#   will be placed and where '{y}' is replaced by the y-coordinate of that center point
#   (for example, 'down 1.5 -2.9' means to place the box held by the robot so that its center point
#   is (1.5,-2.9)).
#
# === Grading ===
#
# - Your planner will be graded against a set of test cases, each equally weighted.
# - Each task will have a "baseline" cost. If your set of moves results in the task being completed
#   with a total cost of K times the baseline cost, you will receive 1/K of the credit for the
#   test case. (Note that if K < 1, this means you earn extra credit!)
# - Otherwise, you will receive no credit for that test case. This could happen for one of several
#   reasons including (but not necessarily limited to):
#   - plan_delivery's moves do not deliver the boxes in the correct order.
#   - plan_delivery's output is not a list of strings in the prescribed format.
#   - plan_delivery does not return an output within the prescribed time limit.
#   - Your code raises an exception.
#
# === Additional Info ===
#
# - You may add additional classes and functions as needed provided they are all in the file `partB.py`.
# - Upload partB.py to Project 2 on T-Square in the Assignments section. Do not put it into an
#   archive with other files.
# - Ask any questions about the directions or specifications on Piazza.
#


import heapq
import math
import robot

class DeliveryPlanner:
    #
    # The following initialization code was copied from the test suite
    #
    BOX_DIAG = 0.1414
    ROBOT_RADIUS = 0.25
    OBSTACLE_DIAG = 1.414
    MOVE_COST = 1.0
    BOX_LIFT_COST = 2.0
    BOX_DOWN_COST = 1.5
    ILLEGAL_MOVE_PENALTY = 100.
    BOX_SIZE = 0.1        # 1/2 of box width and height
    BOX_DIAG = 0.1414
    ROBOT_RADIUS = 0.25
    OBSTACLE_DIAG = 1.414
    ROBOT_STEP_SIZE = 0.1

    def __init__(self, warehouse, todo, max_distance, max_steering):
        # Make sure to deep copy the warehouse
        self.warehouse = list(warehouse)
        # todo is now a list of coordinate locations
        self.todo = todo
        self.rows = len(self.warehouse)
        self.cols = len(self.warehouse[0])

        self.max_distance = max_distance
        self.max_steering = max_steering

        self._set_initial_state_from(warehouse, todo)

    def plan_delivery(self):

        moves = []
        dropzone_c = (self.dropzone['ctr_x'], self.dropzone['ctr_y'])
        current_state = (dropzone_c, 0.0)

        for goal_id, goal_location in enumerate(self.todo):
            path, final_state = self.a_star(current_state, goal_location, goal_id, True)
            optimized_path = self.coalesce_moves(path)
            for move in optimized_path:
                z = "move {} {}".format(move[0], move[1])
                moves.append(z)
            z = "lift {}".format(goal_id)
            moves.append(z)

            current_state = final_state
            # Reverse the path back to the dropzone
            path, final_state = self.a_star(current_state, dropzone_c, goal_id, False)
            optimized_path = self.coalesce_moves(path)
            for move in optimized_path:
                z = "move {} {}".format(move[0], move[1])
                moves.append(z)
            z = "down {} {}".format(final_state[0][0], final_state[0][1])
            moves.append(z)
            current_state = final_state

        return moves

    def a_star(self, start_state, goal, goal_id, is_lift):
        heuristic = robot.compute_distance

        start, initial_bearing = start_state
        goal_x, goal_y = goal

        if is_lift and self.attempt_lift(start, goal, goal_id):
            # print "Lift Goal reachable from start location"
            return [], start_state
        if not is_lift and self.attempt_down(start, goal):
            # print "Down Goal reachable from start location"
            return [], start_state

        frontier = PriorityQueue()
        frontier.append((heuristic(start, goal), start, initial_bearing))
        explored = set()

        parent_node = {}
        parent_node_metadata = {}
        g_cost_to_node = {}
        g_cost_to_node[start_state] = 0

        found_goal = False
        final_state = None

        while frontier.size() > 0:
            _, current_node, bearing = frontier.pop()

            if is_lift and self.attempt_lift(current_node, goal, goal_id):
                final_state = (current_node, bearing)
                found_goal = True
                break

            if not is_lift and self.attempt_down(current_node, goal):
                final_state = (current_node, bearing)
                found_goal = True
                break

            # Neighbors is the tuple of: location, steering, distance, cost
            current_neighbors = self.get_adjacent_neighbors(current_node, bearing, goal, is_lift)

            current_metadata = (current_node, bearing)
            explored.add(current_metadata)
            for neighbor, steering, distance, cost in current_neighbors:
                path_metadata = (steering, distance)
                new_bearing = robot.truncate_angle(bearing + steering)
                neighbor_metadata = (neighbor, new_bearing)
                if neighbor_metadata not in explored and neighbor_metadata not in frontier:
                    # First time seeing the node
                    new_g_cost = g_cost_to_node[current_metadata] + cost
                    g_cost_to_node[neighbor_metadata] = new_g_cost
                    new_h_cost = heuristic(neighbor, goal)

                    frontier.append((new_g_cost + new_h_cost, neighbor, new_bearing))
                    parent_node[neighbor_metadata] = current_metadata
                    parent_node_metadata[neighbor_metadata] = path_metadata
                elif neighbor_metadata in frontier: # Dunder contains is overridden so this works
                    # h costs cancel out so they can be ignored when comparing
                    # the incumbent and new f costs
                    incumbent_cost = g_cost_to_node[neighbor_metadata]
                    new_child_cost = g_cost_to_node[current_metadata] + cost
                    if new_child_cost < incumbent_cost:
                        frontier.remove(neighbor, new_bearing) # Remove by node ID lookup
                        g_cost_to_node[neighbor_metadata] = new_child_cost
                        frontier.append((new_child_cost + heuristic(neighbor, goal),
                            neighbor, new_bearing))
                        # Overwrite a new parent node for the current node
                        parent_node[neighbor_metadata] = current_metadata
                        parent_node_metadata[neighbor_metadata] = path_metadata

        # if not found_goal:
           # raise ValueError("Did not find goal in the map")

        previous_node = final_state
        path_to_goal = [parent_node_metadata[previous_node]]
        while True:
            previous_node = parent_node[previous_node]
            if previous_node == start_state:
                break
            metadata = parent_node_metadata[previous_node]
            path_to_goal = [metadata] + path_to_goal

        return path_to_goal, final_state

    def get_adjacent_neighbors(self, point, bearing, goal, is_lift):
        delta = [[-1, 0], # go up
                 [ 0,-1], # go left
                 [ 1, 0], # go down
                 [ 0, 1], # go right
                 [-1, -1], # go up-left
                 [-1, 1], # go up-right
                 [1, -1], # go down-left
                 [1, 1]] # go down-right

        x_c, y_c = point
        # Merge these two into one function?
        x_m, y_m = self.get_top_left_coordinate_matrix(self.convert_to_matrix_coordinates(point))
        neighbors = set()

        robot_tester = robot.Robot(
                x_c, y_c, bearing, self.max_distance, self.max_steering)
        self.robot = robot_tester

        goal_x_m, goal_y_m = self.get_top_left_coordinate_matrix(
                self.convert_to_matrix_coordinates(goal))

        # Is there a direct path to the goal?
        # If so, take it!

        # Let's see how far the center of that cell is
        # Make sure x2, y2 are the centers of the adjacent cells
        delta_steering = robot.compute_bearing(point, goal)
        steering_raw = delta_steering - bearing
        steering = robot.truncate_angle(steering_raw)

        steering_ok = (-self.max_steering) <= steering <= self.max_steering
        if steering_ok:
            # Now check for distance
            distance_to_goal = robot.compute_distance(point, goal)
            if is_lift:
                # Only need to be within 0.5 to pick it up
                distance_to_goal -= 0.4

            if distance_to_goal <= self.max_distance:
                # Finally make sure the path is clear for the chosen
                # steering and distance
                path_is_traversable, clear_distance = self._is_traversable(
                        goal,
                        distance_to_goal,
                        steering)
                if path_is_traversable:
                    cost = 1.0 + distance_to_goal
                    x2_clear_c, y2_clear_c = self.robot.find_next_point(
                            steering, distance_to_goal)
                    neighbor = ((x2_clear_c, y2_clear_c), steering, distance_to_goal, cost)
                    neighbors.add(neighbor)

        for i in range(len(delta)):
            x2_m = x_m + delta[i][0]
            y2_m = y_m + delta[i][1]
            if x2_m >= 0 and x2_m < self.rows and y2_m >= 0 and y2_m < self.cols:
                cell_value = self.warehouse[x2_m][y2_m]
                if not cell_value == '@' and not cell_value == '.':
                    continue

                neighbor = None
                # Centers are your "north stars". If not in a center
                # find the closest/adjacent ones which are unvisited
                # and go there.

                x2_m_c, y2_m_c = self.get_center_for_matrix_corner((x2_m, y2_m))
                x2_c, y2_c = self.convert_to_cartesian_coordinates((x2_m_c, y2_m_c))

                # Let's see how far the center of that cell is
                # Make sure x2, y2 are the centers of the adjacent cells
                delta_steering = robot.compute_bearing(point, (x2_c, y2_c))
                steering_raw = delta_steering - bearing
                steering = robot.truncate_angle(steering_raw)

                steering_ok = (-self.max_steering) <= steering <= self.max_steering
                if steering_ok:
                    # Now check for distance
                    distance_raw = robot.compute_distance(point, (x2_c, y2_c))
                    # Go the max distance possible
                    distance = min(distance_raw, self.max_distance)

                    # Do I need this if-statement check?
                    if distance > 0.0 :
                        # Finally make sure the path is clear for the chosen
                        # steering and distance
                        path_is_traversable, clear_distance = self._is_traversable(
                                (x2_c, y2_c),
                                distance,
                                steering)
                        if path_is_traversable:
                            cost = 1.0 + distance
                            neighbor = ((x2_c, y2_c), steering, distance, cost)
                            neighbors.add(neighbor)
                        elif goal_x_m == x2_m and goal_y_m == y2_m:
                            # We found the goal cell!
                            if clear_distance > 0.10:
                                # Don't get too close to the goal!
                                clear_distance -= 0.10
                                cost = 1.0 + clear_distance
                                x2_clear_c, y2_clear_c = self.robot.find_next_point(
                                        steering, clear_distance)
                                neighbor = ((x2_clear_c, y2_clear_c), steering,
                                        clear_distance, cost)
                                neighbors.add(neighbor)
                else:
                    # In this case the distance is 0
                    # To lower cost of simply 1 for the turn
                    # and no need to move any directions just change heading
                    if steering > 0.0:
                        # In the calling function, no need to explore neighbors,
                        # just the same position with new heading
                        # Give it the max steering and hope for the best
                        neighbor = ((x_c, y_c), self.max_steering, 0.0, 1.0)
                    if steering < 0.0:
                        # Give it the min steering and hope for the best
                        neighbor = ((x_c, y_c), -self.max_steering, 0.0, 1.0)
                    neighbors.add(neighbor)

        return neighbors

    def coalesce_moves(self, path):
        if len(path) == 0:
            return path

        optimized_path = []
        accumulated_distance = 0.0
        previous_distance = None

        for index, value in reversed(list(enumerate(path))):
            steering, distance = value
            is_first_value = index == 0
            distance_is_valid = True

            if not is_first_value:
                _, prev_distance = path[index - 1]
                if (accumulated_distance + distance + prev_distance) > self.max_distance:
                    distance_is_valid = False

            if steering == 0.0 and not is_first_value and distance_is_valid:
                # Combine distances
                accumulated_distance += distance
            else:
                new_distance = distance + accumulated_distance
                optimized_path.append((steering, new_distance))
                accumulated_distance = 0.0

        return reversed(optimized_path)

    def convert_to_matrix_coordinates(self, point):
        i, j  = point
        return -j, i

    def convert_to_cartesian_coordinates(self, point):
        i, j = point
        return j, -i

    def get_center_for_matrix_corner(self, point):
        x2_m, y2_m = point
        return x2_m + 0.5, y2_m + 0.5

    def find_dropzone_matrix(self, warehouse):
        # Find the dropzone and save it
        for row in range(self.rows):
            for col in range(self.cols):
                if warehouse[row][col] == "@":
                    return (row, col)

    def get_top_left_coordinate_matrix(self, goal_location):
        g_x, g_y = goal_location
        return int(math.floor(g_x)), int(math.floor(g_y))


    #
    # The following initialization code was copied from the test suite
    #
    def _set_initial_state_from(self, warehouse, todo):

        rows = len(warehouse)
        cols = len(warehouse[0])

        self.dropzone = dict()
        self.boxes = dict()
        self.obstacles = []

        # set the warehouse limits:  min_x = 0.0,  max_y = 0.0
        self.warehouse_limits = {'max_x':float(cols), 'min_y':float(-rows)}

        self.warehouse_limits['segments'] = []
        # West segnent (x0,y0) -> (x1,y1)
        self.warehouse_limits['segments'].append(
                    [( self.ROBOT_RADIUS, 0.0 ),
                     ( self.ROBOT_RADIUS, self.warehouse_limits['min_y'] ) ] )
        # South segment
        self.warehouse_limits['segments'].append(
                    [( 0.0,                            self.warehouse_limits['min_y'] + self.ROBOT_RADIUS ),
                     ( self.warehouse_limits['max_x'], self.warehouse_limits['min_y'] + self.ROBOT_RADIUS) ] )
        # East segment
        self.warehouse_limits['segments'].append(
                    [( self.warehouse_limits['max_x'] - self.ROBOT_RADIUS, self.warehouse_limits['min_y'] ),
                     ( self.warehouse_limits['max_x'] - self.ROBOT_RADIUS, 0.0 ) ] )
        # North segment
        self.warehouse_limits['segments'].append(
                    [( self.warehouse_limits['max_x'], -self.ROBOT_RADIUS) ,
                     ( 0.0,                            -self.ROBOT_RADIUS) ] )


        for i in range(rows):
            for j in range(cols):
                this_square = warehouse[i][j]
                x,y = float(j), -float(i)

                # set the obstacle limits, centers, and edges compensated for robot radius
                # precompute these values to save time later
                if this_square == '#':
                    obstacle = dict()

                    # obstacle edges
                    obstacle['min_x'] = x
                    obstacle['max_x'] = x + 1.0
                    obstacle['min_y'] = y - 1.0
                    obstacle['max_y'] = y

                    # center of obstacle
                    obstacle['ctr_x'] = x + 0.5
                    obstacle['ctr_y'] = y - 0.5

                     # compute clearance parameters for robot
                    obstacle = self._dilate_obstacle_for_robot( obstacle )

                    self.obstacles.append( obstacle )

                # set the dropzone limits
                elif this_square == '@':
                    self.dropzone['min_x'] = x
                    self.dropzone['max_x'] = x + 1.0
                    self.dropzone['min_y'] = y - 1.0
                    self.dropzone['max_y'] = y
                    self.dropzone['ctr_x'] = x + 0.5
                    self.dropzone['ctr_y'] = y - 0.5


        for i in range(len(todo)):
            # set up the parameters for processing the box as an obstacle and for
            # picking it up and setting it down
            box = dict()
            # box edges
            box['min_x'] = todo[i][0] - self.BOX_SIZE
            box['max_x'] = todo[i][0] + self.BOX_SIZE
            box['min_y'] = todo[i][1] - self.BOX_SIZE
            box['max_y'] = todo[i][1] + self.BOX_SIZE

            # center of obstacle
            box['ctr_x'] = todo[i][0]
            box['ctr_y'] = todo[i][1]

            # compute clearance parameters for robot
            box = self._dilate_obstacle_for_robot( box )

            self.boxes[str(i)] = box


        # initialize the robot in the center of the dropzone at a bearing pointing due east
        self.robot = robot.Robot(x=self.dropzone['ctr_x'], y=self.dropzone['ctr_y'], bearing=0.0,
                                 max_distance=self.max_distance, max_steering=self.max_steering)

    #
    # The following function was copied from the test suite
    #
    # Compute the clearance parameters for the robot by dilating the object using
    # a circle the radius of the robot.  The resulting shape is rectangular with
    # rounded corners
    def _dilate_obstacle_for_robot(self, obstacle):

        # line segments dilated for robot intersection
        obstacle['segments'] = []
        # West segnent
        obstacle['segments'].append( [( obstacle['min_x'] - self.ROBOT_RADIUS, obstacle['max_y'] ),\
                                        ( obstacle['min_x'] - self.ROBOT_RADIUS, obstacle['min_y'] ) ] )
        # South segment
        obstacle['segments'].append( [( obstacle['min_x'], obstacle['min_y'] - self.ROBOT_RADIUS),\
                                        ( obstacle['max_x'], obstacle['min_y'] - self.ROBOT_RADIUS ) ] )
        # East segment
        obstacle['segments'].append( [( obstacle['max_x'] + self.ROBOT_RADIUS, obstacle['min_y'] ),\
                                        ( obstacle['max_x'] + self.ROBOT_RADIUS, obstacle['max_y'] ) ] )
        # North segment
        obstacle['segments'].append( [( obstacle['max_x'], obstacle['max_y'] + self.ROBOT_RADIUS),\
                                        ( obstacle['min_x'], obstacle['max_y'] + self.ROBOT_RADIUS ) ] )

        obstacle['corners'] = []
        # NW corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['min_x']
        cornerdef['ctr_y'] = obstacle['max_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['min_x'] - self.ROBOT_RADIUS
        cornerdef['max_x'] = obstacle['min_x']
        cornerdef['min_y'] = obstacle['max_y']
        cornerdef['max_y'] = obstacle['max_y'] + self.ROBOT_RADIUS
        obstacle['corners'].append(cornerdef)

        # SW corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['min_x']
        cornerdef['ctr_y'] = obstacle['min_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['min_x'] - self.ROBOT_RADIUS
        cornerdef['max_x'] = obstacle['min_x']
        cornerdef['min_y'] = obstacle['min_y'] - self.ROBOT_RADIUS
        cornerdef['max_y'] = obstacle['min_y']
        obstacle['corners'].append(cornerdef)

        # SE corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['max_x']
        cornerdef['ctr_y'] = obstacle['min_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['max_x']
        cornerdef['max_x'] = obstacle['max_x'] + self.ROBOT_RADIUS
        cornerdef['min_y'] = obstacle['min_y'] - self.ROBOT_RADIUS
        cornerdef['max_y'] = obstacle['min_y']
        obstacle['corners'].append(cornerdef)

        # NE corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['max_x']
        cornerdef['ctr_y'] = obstacle['max_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['max_x']
        cornerdef['max_x'] = obstacle['max_x'] + self.ROBOT_RADIUS
        cornerdef['min_y'] = obstacle['max_y']
        cornerdef['max_y'] = obstacle['max_y'] + self.ROBOT_RADIUS
        obstacle['corners'].append(cornerdef)

        return obstacle


    #
    # The following function was copied from the test suite
    #
    # Check the path to make sure the robot can traverse it without running into
    # boxes, obstacles, or the warehouse walls
    def _is_traversable(self, destination, distance, steering):

        NUDGE_DISTANCE = 0.01

        # end points of trajectory
        t1 = destination
        t0 = (self.robot.x, self.robot.y)
        # the distance to check against
        chk_distance = distance


        # Is the path too close to any box
        min_distance_to_intercept = chk_distance
        for box_id, box in self.boxes.iteritems():
            # do a coarse check to see if the center of the obstacle
            # is too far away to be concerned with
            dst = self._distance_point_to_line_segment( (box['ctr_x'], box['ctr_y']), t0, t1 )
            if (dst <= self.BOX_DIAG + self.ROBOT_RADIUS):
                # refine the intercept computation
                dst, intercept_point = self._check_intersection(t0,t1,box)
                if dst < min_distance_to_intercept:
                    min_distance_to_intercept = dst
                    min_intercept_point = intercept_point

        # Is the path too close to any obstacle
        for o in self.obstacles :
            # do a coarse check to see if the center of the obstacle
            # is too far away to be concerned with
            dst = self._distance_point_to_line_segment( (o['ctr_x'],o['ctr_y']), t0, t1 )
            if (dst <= self.OBSTACLE_DIAG + self.ROBOT_RADIUS):
                # refine the intercept computation
                dst, intercept_point = self._check_intersection(t0,t1,o)
                if dst < min_distance_to_intercept:
                    min_distance_to_intercept = dst
                    min_intercept_point = intercept_point

        # Check the edges of the warehouse
        dst, intercept_point = self._check_intersection(t0,t1,self.warehouse_limits)
        if dst < min_distance_to_intercept:
            min_distance_to_intercept = dst
            min_intercept_point = intercept_point

        if min_distance_to_intercept < chk_distance :
            # print "*** Robot crashed at {p[0]:6.2f} {p[1]:6.2f} ***".format( p = min_intercept_point )
            return False, robot.compute_distance( (self.robot.x, self.robot.y), min_intercept_point )

        return True, distance

    #
    # The following function was copied from the test suite
    #
    # Check if the trajectory intersects with a square obstacle
    def _check_intersection(self,t0,t1,obstacle):

        min_distance_to_intercept = 1.e6
        min_intercept_point = (0.,0.)

        # check each segment
        for s in obstacle['segments']:
            dst, intercept_point =  self._linesegment_intersection( t0, t1, s[0], s[1] )
            if dst < min_distance_to_intercept:
                min_distance_to_intercept = dst
                min_intercept_point = intercept_point

        # if circular corners are defined - check them
        # circular corners occur when dilating a rectangle with a circle
        if obstacle.has_key('corners') :
            for c in obstacle['corners'] :
                dst, intercept_point =  self._corner_intersection( t0, t1, c )
                if dst < min_distance_to_intercept:
                    min_distance_to_intercept = dst
                    min_intercept_point = intercept_point


        return min_distance_to_intercept, min_intercept_point

    #
    # The following function was copied from the test suite
    #
    # Find the intersection of a line segment and a semicircle as defined in
    # the corner dictionary
    # Use quadratic solution to solve simultaneous equations for
    # (x-a)^2 + (y-b)^2 = r^2 and y = mx + c
    def _corner_intersection( self, t0, t1, corner ):

        dst = 1.e6
        intercept_point = (0.,0.)

        # Note:  changing nomenclature here so that circle center is a,b
        # and line intercept is c (not b as above)
        a = corner['ctr_x']                 # circle ctrs
        b = corner['ctr_y']
        r = corner['radius']

        # check the case for infinite slope
        dx = t1[0] - t0[0]

        # Find intersection assuming vertical trajectory
        if abs( dx ) < 1.e-6 :
            x0 = t0[0] - a
            #qa = 1.
            qb = -2.*b
            qc = b*b + x0*x0 - r*r
            disc = qb*qb - 4.*qc

            if disc >= 0.:
                sd = math.sqrt(disc)
                xp = xm = t0[0]
                yp = (-qb + sd)/2.
                ym = (-qb - sd)/2.

        # Find intersection assuming non vertical trajectory
        else:
            m = (t1[1] - t0[1])/dx # slope of line
            c = t0[1] - m*t0[0]    # y intercept of line

            qa = 1.+m*m
            qb = 2.*(m*c - m*b - a)
            qc = a*a + b*b + c*c - 2.*b*c - r*r

            disc = qb*qb - 4.*qa*qc

            if disc >= 0.:
                sd = math.sqrt(disc)
                xp = (-qb + sd) / (2.*qa)
                yp = m*xp + c
                xm = (-qb - sd) / (2.*qa)
                ym = m*xm + c

        if disc >= 0. :
            dp2 = dm2 = 1.e6
            if corner['min_x'] <= xp <= corner['max_x'] and corner['min_y'] <= yp <= corner['max_y'] :
                dp2 = (xp - t0[0])**2 + (yp- t0[1])**2

            if corner['min_x'] <= xm <= corner['max_x'] and corner['min_y'] <= ym <= corner['max_y'] :
                dm2 = (xm - t0[0])**2 + (ym- t0[1])**2

            if dp2 < dm2 :
                # make sure the intersection pointn is actually on the trajectory segment
                if self._distance_point_to_line_segment( (xp,yp), t0, t1 ) < 1.e-6 :
                    dst = math.sqrt(dp2)
                    intercept_point = (xp, yp)
            else :
                if self._distance_point_to_line_segment( (xm,ym), t0, t1 ) < 1.e-6 :
                    dst = math.sqrt(dm2)
                    intercept_point = (xm, ym)


        return dst, intercept_point


    #
    # The following function was copied from the test suite
    #
    # Find the distance from a point to a line segment
    # This function is used primarily to find the distance between a trajectory
    # segment defined by l0,l1 and the center of an obstacle or box specified
    # by point p.  For a reference see the lecture on SLAM and the segmented CTE
    def _distance_point_to_line_segment( self, p, l0, l1 ):

        dst = 1.e6
        dx = l1[0] - l0[0]
        dy = l1[1] - l0[1]

        # check that l0,l1 don't describe a point
        d2 = (dx*dx + dy*dy)

        if abs(d2) > 1.e-6:

            t = ((p[0] - l0[0]) * dx + (p[1] - l0[1]) * dy)/d2

            # if point is on line segment
            if 0.0 <= t <= 1.0:
                intx, inty = l0[0] + t*dx, l0[1] + t*dy
                dx, dy = p[0] - intx, p[1] - inty

            # point is beyond end point
            elif t > 1.0 :
                dx, dy = p[0] - l1[0], p[1] - l1[1]

            # point is before beginning point
            else:
                dx, dy = p[0] - l0[0], p[1] - l0[1]

            dst = math.sqrt(dx*dx + dy*dy)

        else:
            dx, dy = p[0] - l0[0], p[1] - l0[1]
            dst = math.sqrt(dx*dx + dy*dy)


        return dst

    #
    # The following function was copied from the test suite
    #
    # Check for the intersection of two line segments.  This function assumes that the robot
    # trajectory starts at p0 and ends at p1.  The segment being checked is q0 and q1
    # There is no check for colinearity because the obstacle is assumed to have orthogonal
    # sides and so the intersection for a trajectory that is colinear and overlapping with
    # one side will intersect with the orthogonal side, since by definition the starting
    # point of the trajectory is always outside the obstacle.
    # For a reference see:
    #  http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect/565282#565282
    def _linesegment_intersection( self, p0, p1, q0, q1 ):

        eps = 1.0e-6
        dst = 1.0e6
        intersection = (0.,0.)

        r = p1[0]-p0[0], p1[1]-p0[1]
        s = q1[0]-q0[0], q1[1]-q0[1]
        qmp = q0[0]-p0[0], q0[1]-p0[1]

        rxs   = r[0]*s[1] - r[1]*s[0]
        qmpxr = qmp[0]*r[1] - qmp[1]*r[0]

        if abs(rxs) >= eps :

            # check for intersection
            # parametric equations for intersection
            # t = (q - p) x s / (r x s)
            t = (qmp[0]*s[1] - qmp[1]*s[0])/rxs

            # u = (q - p) x r / (r x s)
            u = qmpxr/rxs

            if (0.0 <= t <= 1.0) and (0.0 <= u <= 1.0) :
                dx, dy = t*r[0], t*r[1]
                dst = math.sqrt( dx*dx + dy*dy)
                intersection =  (p0[0] + dx, p0[1] + dy)

        return dst, intersection

    #
    # The following function was copied from the test suite
    #
    def attempt_lift(self, current, box_center, goal_id):
        # - The robot may pick up a box that is within a distance of 0.5 of the robot's center.
        box_id = str(goal_id)
        box_is_adjacent = robot.compute_distance(current, box_center) <= 0.5
        if box_is_adjacent:
            self._lift_box(box_id)
        return box_is_adjacent

    #
    # The following function was copied from the test suite
    #
    def attempt_down(self, current, destination):
        # - The robot may put a box down within a distance of 0.5 of the robot's center.
        #   The cost to set a box down is 1.5 (regardless of the direction in which the robot puts down the box).
        # - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
        #   house.
        # Illegal moves (do not set box down but incur cost):
        # - putting down a box too far away or so that it's touching a wall, the warehouse exterior,
        #   another box, or the robot
        # - putting down a box while not holding a box

        # self._increase_total_cost_by(self.BOX_DOWN_COST)
        # destination = (x,y)

        destination_is_adjacent = robot.compute_distance(current, destination) <= 0.5
        destination_is_open = self._is_open_for_box(destination)
        destination_is_within_warehouse = self._is_box_within_warehouse(destination)

        is_legal_down = (destination_is_adjacent and destination_is_open
                        and destination_is_within_warehouse)

        is_within_dropzone =  self._is_box_within_dropzone(destination)
        if is_legal_down:
            self._down_box(destination)

        return is_legal_down and is_within_dropzone

    #
    # The following function was copied from the test suite
    #
    # Verify a box can be placed at the desired coordinates
    def _is_open_for_box(self, coordinates):

        # process all boxes still remaining
        for b in self.boxes:
            if not self._is_box_outside_obstacle( coordinates, self.boxes[b] ):
                # print '*** Could not set down - box {} in the way ***'.format(b)
                return False

        # process all obstacles
        for o in self.obstacles:
            if not self._is_box_outside_obstacle( coordinates, o ):
                # print '*** Could not set down - obstacle {} in the way ***'.format(0)
                return False

        return True

    #
    # The following function was copied from the test suite
    #
    # Check the box to make sure it is outside an obstacle (or other box)
    def _is_box_outside_obstacle(self, coordinates, obstacle):

        # the center coordinates of the box to be placed
        x, y = coordinates

        if (x - self.BOX_SIZE) >= obstacle['max_x']:
            return True
        elif (x + self.BOX_SIZE) <= obstacle['min_x']:
            return True
        elif (y - self.BOX_SIZE) >= obstacle['max_y']:
            return True
        elif (y + self.BOX_SIZE) <= obstacle['min_y']:
            return True
        else:
            return False

    #
    # The following function was copied from the test suite
    #
    # Assumes the warehouse NW corner is (0,0) and SE corner is (max_x, min_y)
    def _is_box_within_warehouse(self, coordinates):
        x, y = coordinates
        return self.BOX_SIZE < x < (self.warehouse_limits['max_x']-self.BOX_SIZE)  and \
               (self.warehouse_limits['min_y'] + self.BOX_SIZE) < y < -self.BOX_SIZE


    #
    # The following function was copied from the test suite
    #
    # Set the box down at the specified destination and recompute its parameters
    # for obstacle avoidance computation
    def _down_box(self, destination):

        # - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
        #   house.
        x, y = destination

        if self._is_box_within_dropzone(destination):
           # self._deliver_box(self.box_held)
           pass
        else:
            # set up the parameters for processing the box as an obstacle and for
            # picking it up and setting it down later
            box = dict()
            # box edges
            box['min_x'] = x - self.BOX_SIZE
            box['max_x'] = x + self.BOX_SIZE
            box['min_y'] = y - self.BOX_SIZE
            box['max_y'] = y + self.BOX_SIZE

            # center of obstacle
            box['ctr_x'] = x
            box['ctr_y'] = y

            # compute clearance parameters for robot
            box = self._dilate_obstacle_for_robot( box )

            # self.boxes[self.box_held] = box

        # self.box_held = None


    #
    # The following function was copied from the test suite
    #
    # Lift the box
    def _lift_box(self, box_id):
        self.boxes.pop(box_id)

    #
    # The following function was copied from the test suite
    #
    # Check if the box within the dropzone
    def _is_box_within_dropzone(self, coordinates):
        x, y = coordinates
        return (self.dropzone['min_x'] + self.BOX_SIZE) < x < (self.dropzone['max_x'] - self.BOX_SIZE)  and \
               (self.dropzone['min_y'] + self.BOX_SIZE) < y < (self.dropzone['max_y'] - self.BOX_SIZE)

    #
    # The following function was copied from the test suite
    #
    # Deliver the box by appending to the list of delivered boxes
    def _deliver_box(self, box_id):
        self.boxes_delivered.append(box_id)


# This PriorityQueue class implementation is based upon work I originally
# completed and submitted as part of project 2 in CS6601 - Aritificial Intelligence.
# It is a re-usable class and is needed to make up for a lack of a robust
# PriorityQueue implementation in the Python standard library.
class PriorityQueue():

    def __init__(self):
        self.queue = []
        self.current = 0

    def next(self):
        if self.current >= len(self.queue):
            self.current
            raise StopIteration

        out = self.queue[self.current]
        self.current += 1

        return out

    def pop(self):
        top_of_heap = heapq.heappop(self.queue)
        return top_of_heap

    def remove(self, nodeId, bearing):
        for i in range(self.size()):
            if self.queue[i][1] == nodeId and \
                self.queue[i][2] == bearing:
                del self.queue[i]
                heapq.heapify(self.queue)
                break

    def __iter__(self):
        return self

    def __repr__(self):
        return 'PQ:[%s]'%(', '.join([str(i) for i in self.queue]))

    def __str__(self):
        return 'PQ:[%s]'%(', '.join([str(i) for i in self.queue]))

    def append(self, node):
        heapq.heappush(self.queue, node)

    def __contains__(self, key):
        self.current = 0
        return key in [(n,b) for c,n,b in self.queue]

    def __eq__(self, other):
        self.current = 0
        return self == other

    def size(self):
        return len(self.queue)

    def clear(self):
        self.queue = []

    def top(self):
        return self.queue[0]

    __next__ = next


