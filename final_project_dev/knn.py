###############################################################
# Predicts the next i steps given a set of training measurements
#
# class: knn
#
# method: constructor
# input: numpy array of measurements in the following format
#   [x, y, distance, bearing, turning]
# output: none
#
# method: get_predictions
# input: number of steps to predict
# output: numpy array with predicted positions in the format:
#   [x, y, distance, bearing]
###############################################################



import numpy as np
import sklearn.neighbors as sk

import sklearn.model_selection as ms
import math

class knn:
    def __init__(self, measurements):
        self.train_set = measurements
        self.train_data = self.train_set[0:-2,2:4]
        classes = self.train_data[1:,0:2]
        self.train_data = self.train_data[:-1,:]
        for i in range(2):
            self.train_data = np.insert(self.train_data, len(self.train_data[0]), 0, axis=1)
        self.train_data[:,2:] = classes
        # REFERENCE: Based on cross validation sample located at
        # http://scikit-learn.org/stable/modules/cross_validation.html
        # self.knn = sk.KNeighborsRegressor(n_neighbors=11, weights='distance')
        # self.knn = sk.KNeighborsRegressor()
        self.knn = sk.KNeighborsRegressor(n_neighbors=10, weights='distance')        
        self.knn.fit(self.train_data[:,0:2], self.train_data[:,2:4])        
        # print self.train_set
        # print self.train_data

    def get_predictions(self, starting_point, last_bearing, n_predictions):

        # print 'last_prediction', last_prediction
        predictions = []
        avg_data = np.mean(self.train_data[-20:-2,:],axis=0)
        print 'avg_data', avg_data
        for i in range(n_predictions):
            # print 'last_prediction', last_prediction
            # print 'last_bearing', last_bearing
            # print self.train_data[-2:,:]
            next_bearing = avg_data[0:2] #self.knn.predict([last_bearing])
            # next_bearing = next_bearing[0]
            v, b = next_bearing[0], next_bearing[1]
            # print starting_point

            # print prediction1
            # prediction = prediction[0]
            # print 'prediction', prediction
            starting_point[0] = starting_point[0] + v*math.cos(b)
            starting_point[1] = starting_point[1] - v*math.sin(b)
            predictions.append(starting_point.tolist())
            last_bearing = next_bearing
        return predictions



