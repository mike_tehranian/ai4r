# Example of UKF

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import math
import random
import numpy as np


# measurement function: reflect the fact that we observe x, y, heading,
# turning
H = matrix([[1., 0., 0., 0.],
            [0., 1., 0., 0.],
            [0., 0., 1., 0.],
            [0., 0., 1., 0.],
            [0., 0., 0., 1.]])
# measurement uncertainty
R = matrix([[1., 0., 0., 0.],
            [0., 1., 0., 0.],
            [0., 0., pi, 0.],
            [0., 0., 0., pi/10]])
# 4D identity matrix
I = matrix([[1., 0., 0., 0.],
            [0., 1., 0., 0.],
            [0., 0., 1., 0.],
            [0., 0., 0., 1.]])

def Q_model_uncertainty(dt=1., var=1.):
    """ Returns the Q matrix for the Discrete Constant White Noise
    Model. dim is 5, dt is the time step, and sigma is the
    variance in the noise.
    Q is computed as the G * G^T * variance, where G is the process noise per
    time step. In other words, G = [[.5dt^2][dt]]^T for the constant velocity
    model.
    Parameters
    -----------
    dt : float, default=1.0
        time step in whatever units your filter is using for time. i.e. the
        amount of time between innovations
    var : float, default=1.0
        variance in the noise
    """
    Q = np.array([[0.05*dt, 0., 0., 0., 0.],
                 [0., 0.05*dt, 0., 0., 0.],
                 [0., 0., 0.01*dt, 0., 0.],
                 [0., 0., 0., 0.01*dt, 0.],
                 [0., 0., 0., 0., 0.05*dt]], dtype=float)
    return Q * var


def R_measurement_noise(dt=1., var=1.):
    """
    Parameters
    -----------
    dt : float, default=1.0
        time step in whatever units your filter is using for time. i.e. the
        amount of time between innovations
    var : float, default=1.0
        variance in the noise
    """
    R = np.array([[1.*dt, 0., 0., 0., 0.],
                 [0., 1.*dt, 0., 0., 0.],
                 [0., 0., math.pi*dt, 0., 0.],
                 [0., 0., 0., math.pi*dt, 0.],
                 [0., 0., 0., 0., 1.*dt]], dtype=float)
    return R * var


if __name__ == "__main__":
    dt = 1.

    # initial state: x, y, heading, and turning
    state = matrix([[0.], [0.], [0.], [0.]])
    # initial uncertainty: 100 for positions x, y. pi for heading, turning
    covariance = matrix([
                [100., 0., 0., 0.],
                [0., 100., 0., 0.],
                [0., 0., pi, 0.],
                [0., 0., 0., pi]])

    filter_state = []
    for z in measurements:
        extended_kalman_filter(state, covariance, z, distance, H, R, I, Q)
        filter_state.append(ukf.x.copy())
    filter_state = np.array(filter_state)

    plt.plot(filter_state[:, 0], filter_state[:, 1])
    print('UKF standard deviation {:.3f} meters'.format(np.std(filter_state - measurements)))


def extended_kalman_filter(x, P, new_measurement, distance, H, R, I, Q):
    # Update
    # Measurement function H is linear
    Z = matrix([new_measurement])
    # error: very important term
    y = Z.transpose() - (H * x)
    # residual covariance
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)
    P = (I - (K * H)) * P

    # Predict
    # Use non-linear g(x) and the Jacobian G
    x_predict = g(x, distance)
    G = calculate_jacobian(x_predict, distance)
    P_predict = G * P * G.transpose() + Q

    return x_predict, P_predict

def g(state, distance):
    x, y, heading, turning = [state.value[i][0] for i in range(state.dimx)]

    new_heading = angle_trunc(heading + turning)

    # Nonlinear State Equations:
    x = x + distance * cos(new_heading)
    y = y + distance * sin(new_heading)
    heading = new_heading
    turning = turning

    return matrix([[x], [y], [heading], [turning]])

def calculate_jacobian(state, distance):
    x, y, heading, turning = [state.value[i][0] for i in range(state.dimx)]

    # State equations:
    # x_t = x_t-1 + distance*cos(heading + turning)
    # y_t = y_t-1 + distance*sin(heading + turning)
    # heading_t = heading_t-1 + turning
    # turning_1 = turning_t-1
    dx_dtheta = -distance * sin(heading + turning)
    dy_dtheta = distance * cos(heading + turning)

    # Jacobian of the State Equations
    G = matrix([[1., 0., dx_dtheta, dx_dtheta],
                [0., 1., dy_dtheta, dy_dtheta],
                [0., 0., 1., 1.],
                [0., 0., 0., 1.]])
    return G
