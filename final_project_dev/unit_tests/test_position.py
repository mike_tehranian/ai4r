# REFERENCE: Based on sample in https://docs.python.org/2/library/unittest.html 
# Started by copying sample test case and then made changes to it

import unittest
from measurement import *
from position import *

class TestPosition(unittest.TestCase):

    def setUp(self):
        self.test_file = 'unit_tests/test_position.txt'

    def test_simple_file(self):
        meas =  get_measurements(self.test_file)
        for i in range(1, len(meas)-1):
            pos = get_next_position(meas[i-1,0], meas[i-1,1], meas[i-1,2], meas[i-1,3])
            self.assertAlmostEqual(meas[i,0], pos[0])
            self.assertAlmostEqual(meas[i,1], pos[1])

        
if __name__ == '__main__':
    unittest.main()