# REFERENCE: Based on sample in https://docs.python.org/2/library/unittest.html 
# Started by copying sample test case and then made changes to it

import unittest
import knn as ml
from measurement import *
import sklearn.neighbors as sk
import sklearn.model_selection as ms
#http://scikit-learn.org/stable/modules/generated/sklearn.gaussian_process.GaussianProcessRegressor.html#sklearn.gaussian_process.GaussianProcessRegressor
import sklearn.linear_model as lm
import sklearn.tree as tr

# This just tests to make sure that the class will return some predictions
# so it doesn't test accuracy
# It is also servers as a sample of how to use the knn class
class TestKnn(unittest.TestCase):

    def setUp(self):
        self.training_data = 'data/training_data.txt'

    # REFERENCE: Based on cross validation sample located at
    # http://scikit-learn.org/stable/modules/cross_validation.html
    def test_cross_validate(self):
        meas =  get_measurements(self.training_data)
        k = ml.knn(meas)
        knn = sk.KNeighborsRegressor(n_neighbors=10, weights='distance')
        cv = ms.cross_val_score(knn, k.train_data[:,0:4], k.train_data[:,4:], cv=10)
        # REFERENCE: copied this line verbatime from
        # http://scikit-learn.org/stable/modules/cross_validation.html#cross-validation
        print("KNN Accuracy: %0.2f (+/- %0.2f)" % (cv.mean(), cv.std() * 2))
        
        ####################################################
        # WARNING: All of these accuracies are wrong!!!!!! #
        ####################################################
        
        rs = lm.RANSACRegressor() #huber regressor will also work
        cv = ms.cross_val_score(rs, k.train_data[:,0:2], k.train_data[:,4:6], cv=10)
        # REFERENCE: copied this line verbatime from
        # http://scikit-learn.org/stable/modules/cross_validation.html#cross-validation
        print("Ransac Accuracy: %0.2f (+/- %0.2f)" % (cv.mean(), cv.std() * 2))

        tree = tr.DecisionTreeRegressor()
        cv = ms.cross_val_score(tree, k.train_data[:,0:4], k.train_data[:,4:], cv=10)
        # REFERENCE: copied this line verbatime from
        # http://scikit-learn.org/stable/modules/cross_validation.html#cross-validation
        print("Tree Accuracy: %0.2f (+/- %0.2f)" % (cv.mean(), cv.std() * 2))


        
if __name__ == '__main__':
    unittest.main()