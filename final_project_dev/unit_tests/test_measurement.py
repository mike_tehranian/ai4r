# REFERENCE: Based on sample in https://docs.python.org/2/library/unittest.html 
# Started by copying sample test case and then made changes to it

import unittest
from measurement import *

class TestMeasurement(unittest.TestCase):

    def setUp(self):
        self.training_file = 'train/train01.txt'

    def test_dist(self):
        meas =  get_measurements(self.training_file)
        # print meas[0,2]
        self.assertAlmostEqual(meas[0,2], 15.264, places=3)

    def test_bearing(self):
        meas =  get_measurements(self.training_file)
        # print meas[0,2]
        self.assertAlmostEqual(meas[0,3], 1.019, places=3)

    def test_turning(self):
        meas =  get_measurements(self.training_file)
        # print meas[0,2]
        self.assertAlmostEqual(meas[0,4], -0.324, places=3)

    def test_simple_file(self):
        meas =  get_measurements('unit_tests/test_measurement.txt')
        self.assertAlmostEqual(meas[0,0], 1., places=3)
        self.assertAlmostEqual(meas[0,1], 1., places=3)
        self.assertAlmostEqual(meas[1,0], 2., places=3)
        self.assertAlmostEqual(meas[1,1], 2., places=3)
        self.assertAlmostEqual(meas[2,0], 3., places=3)
        self.assertAlmostEqual(meas[2,1], 1., places=3)
        self.assertAlmostEqual(meas[3,0], 3., places=3)
        self.assertAlmostEqual(meas[3,1], 2., places=3)
        self.assertAlmostEqual(meas[4,0], 2., places=3)
        self.assertAlmostEqual(meas[4,1], 3., places=3)

        self.assertAlmostEqual(meas[0,2], math.sqrt(2.), places=3)
        self.assertAlmostEqual(meas[0,3], (2.*math.pi)-(math.pi/4.), places=3)
        self.assertAlmostEqual(meas[1,2], math.sqrt(2.), places=3)
        self.assertAlmostEqual(meas[1,3], (math.pi/4.), places=3)
        self.assertAlmostEqual(meas[0,4], (math.pi/2.), places=3)
        self.assertAlmostEqual(meas[2,2], 1., places=3)
        self.assertAlmostEqual(meas[2,3], (3.*math.pi/2.), places=3)
        self.assertAlmostEqual(meas[1,4], (-3.*math.pi/4.), places=3)
        self.assertAlmostEqual(meas[3,2], math.sqrt(2.), places=3)
        self.assertAlmostEqual(meas[3,3], (5.*math.pi/4.), places=3)

        
    def test_print_measurement(self):

        # REFERENCE: https://stackoverflow.com/questions/9777783/suppress-scientific-notation-in-numpy-when-creating-array-from-nested-list
        np.set_printoptions(suppress=True)

        meas =  get_measurements(self.training_file)
        # meas =  get_measurements('test_measurement.txt')

        # print np.round(meas[0:10,], 3)

if __name__ == '__main__':
    unittest.main()