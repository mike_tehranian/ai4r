# REFERENCE: Based on sample in https://docs.python.org/2/library/unittest.html 
# Started by copying sample test case and then made changes to it

import unittest
import knn as ml
from measurement import *
import sklearn.neighbors as sk
import sklearn.model_selection as ms

# This just tests to make sure that the class will return some predictions
# so it doesn't test accuracy
# It is also servers as a sample of how to use the knn class
class TestKnn(unittest.TestCase):

    def setUp(self):
        self.test_file = 'inputs/train01.txt'

    # def test_simple_file(self):
    #     meas =  get_measurements(self.test_file)
    #     knn = ml.knn(meas)
    #     predictions = knn.get_predictions(60)
    #     # print predictions

    # REFERENCE: Based on cross validation sample located at
    # http://scikit-learn.org/stable/modules/cross_validation.html
    def test_cross_validate(self):
        meas =  get_measurements('data/training_data.txt')
        meas =  get_measurements(self.test_file)
        k = ml.knn(meas)
        knn = sk.KNeighborsRegressor(n_neighbors=20)#, weights='distance')
        # knn.fit(k.train_data[:,2:4], k.train_data[:,6:8]) 
        # print knn.predict(k.train_data[0,2:4])
        # print k.train_data[0,:]       
        cv = ms.cross_val_score(knn, k.train_data[:,2:4], k.train_data[:,6:8], cv=10)

        # REFERENCE: copied this line verbatime from
        # http://scikit-learn.org/stable/modules/cross_validation.html#cross-validation
        print("Accuracy: %0.2f (+/- %0.2f)" % (cv.mean(), cv.std() * 2))

        # with open('measurements.txt', 'w') as f:
        #     for i in range(len(meas)):
        #         o = np.round(meas[i,:],3)
        #         print >> f, '{}'.format(o)

        # print meas
        # meas[np.isnan(meas)] = 0
        # meas[np.isinf(meas)] = 10000


        # knn = sk.KNeighborsRegressor(n_neighbors=11, weights='distance')
        # # knn = sk.KNeighborsRegressor()


        
if __name__ == '__main__':
    unittest.main()