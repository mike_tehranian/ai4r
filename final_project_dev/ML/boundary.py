import numpy as np
import math
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

##########################################################################
# Estimates box and candle boundaries based on motion from training data #
##########################################################################

# Load training data
f = open('original_data/training_data.txt', 'r')
data = []
for line in f.readlines():
    x, y = line.strip().split(',')
    data.append((int(x), int(y)))
data = np.array(data)

##################
# CALCULATE BOX  #
##################

# Data boundaries
x_range = range(65, 564, 1)
y_range = range(31, 327, 1)

# Initialize arrays
min_x = []
min_y = []
max_x = []
max_y = []

for x in x_range:
    y_value = data[np.where(data[:, 0] == x), 1]

    try:
        min_y.append((x, int(np.min(y_value))))
        max_y.append((x, int(np.max(y_value))))
    except ValueError:
        pass  # No data

for y in y_range:
    x_value = data[np.where(data[:, 1] == y), 0]

    try:
        min_x.append((int(np.min(x_value)), y))
        max_x.append((int(np.max(x_value)), y))
    except ValueError:
        pass  # No data

min_x = np.array(min_x)
min_y = np.array(min_y)
max_x = np.array(max_x)
max_y = np.array(max_y)

# Remove outliers that are +/- 20 units away from current median, before keeping 3 sigma
min_x = min_x[(min_x[:, 0] > np.median(min_x[:, 0]) - 20) & \
              (min_x[:, 0] < np.median(min_x[:, 0]) + 20), :]
min_x = min_x[(min_x[:, 0] < np.median(min_x[:, 0]) + np.std(min_x[:, 0]) * 0), :]

max_x = max_x[(max_x[:, 0] > np.median(max_x[:, 0]) - 20) & \
              (max_x[:, 0] < np.median(max_x[:, 0]) + 20), :]
max_x = max_x[(max_x[:, 0] > np.median(max_x[:, 0]) - np.std(max_x[:, 0]) * 0), :]

min_y = min_y[(min_y[:, 1] > np.median(min_y[:, 1]) - 20) & \
              (min_y[:, 1] < np.median(min_y[:, 1]) + 20), :]
min_y = min_y[(min_y[:, 1] < np.median(min_y[:, 1]) + np.std(min_y[:, 1]) * 0), :]

max_y = max_y[(max_y[:, 1] > np.median(max_y[:, 1]) - 20) & \
              (max_y[:, 1] < np.median(max_y[:, 1]) + 20), :]
max_y = max_y[(max_y[:, 1] > np.median(max_y[:, 1]) - np.std(max_y[:, 1]) * 0), :]

# Output statistics
box_min_x = np.median(min_x[:, 0]) - 2 * np.std(min_x[:, 0])
box_max_x = np.median(max_x[:, 0]) + 2 * np.std(max_x[:, 0])
box_min_y = np.median(min_y[:, 1]) - 2 * np.std(min_y[:, 1])
box_max_y = np.median(max_y[:, 1]) + 2 * np.std(max_y[:, 1])
print "x min/max average: %s / %s" % (box_min_x, box_max_x)
print "y min/max average: %s / %s" % (box_min_y, box_max_y)

# Show min/max points & bounding box

plt.plot(min_x[:, 0], min_x[:, 1], 'ro')
plt.plot(max_x[:, 0], max_x[:, 1], 'yo')
plt.plot(min_y[:, 0], min_y[:, 1], 'go')
plt.plot(max_y[:, 0], max_y[:, 1], 'bo')

plt.plot([box_min_x, box_min_x], [box_min_y, box_max_y], 'k', linewidth=3.0)  # Left wall
plt.plot([box_max_x, box_max_x], [box_min_y, box_max_y], 'k', linewidth=3.0)  # Right wall
plt.plot([box_min_x, box_max_x], [box_min_y, box_min_y], 'k', linewidth=3.0)  # Top wall per the video
plt.plot([box_min_x, box_max_x], [box_max_y, box_max_y], 'k', linewidth=3.0)  # Bottom wall per the video

#####################
#  CALCULATE CANDLE #
#####################

x = (box_max_x - box_min_x) / 2 + box_min_x
x = 333
y = (box_max_y - box_min_y) / 2 + box_min_y
y = 178

r_per_deg = [float('inf')] * 72  # distance per 5 degree
xy_per_deg = [None] * len(r_per_deg)
for xy in data:
    theta = int(math.floor(math.atan2(xy[1] - y, xy[0] - x) * 180 / 5 / np.pi))
    r = math.sqrt((xy[1] - y) ** 2 + (xy[0] - x) ** 2)

    if r < r_per_deg[theta]:
        r_per_deg[theta] = r
        xy_per_deg[theta] = xy

xy_per_deg = np.array(xy_per_deg)

radius = np.median(r_per_deg)

plt.plot(x, y, 'ko')
plt.plot(xy_per_deg[:, 0], xy_per_deg[:, 1], 'co')

print "circle x/y/r: %s/%s/%s" % (x, y, radius)

circle = plt.Circle((x, y), radius, fill=False, color='k', linewidth=3.0)
fig = plt.gcf()
ax = fig.gca()
ax.add_artist(circle)

plt.show()
