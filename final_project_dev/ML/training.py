import numpy as np
import math
import matplotlib.pyplot as plt
from feature_creation import get_features, plot_data_with_boundaries, N_FRAMES
from crash_library import check_crashing
from motion_variables import *

def error(l1, l2):
    return sum((c - a)**2 + (d - b)**2 for ((a, b), (c, d)) in zip(l1, l2))**0.5

# Load training data
filename = 'training_data'
f = open('features/' + filename + '.txt', 'r')
data = []
for line in f.readlines():
    row = line.strip().split(',')
    data.append([float(e) for e in row])
f.close()
data = np.array(data)

scale_mean = np.mean(data[:, 2:-2], axis=0)
scale_std = np.std(data[:, 2:-2], axis=0)

data[:,2] = (data[:,2] - scale_mean[0]) / scale_std[0]
data[:,3] = (data[:,3] - scale_mean[1]) / scale_std[1]
data[:,4] = (data[:,4] - scale_mean[2]) / scale_std[2]


pos = data[:, 0:2]
X = data[:, 2:-2] # [boundary_distance, last_angle, last_velocity]
y = data[:, -2:] # [next_velocity, next_added_angle]


# Loading test data
test_data = []
for i in range(1, 11, 1):
    filename = 'test%02d' % i

    f = open('features/' + filename + '.txt', 'r')
    tmp_data = []
    for line in f.readlines():
        row = line.strip().split(',')
        tmp_data.append([float(e) for e in row])
    f.close()
    tmp_data = np.array(tmp_data)

    tmp_data[:, 2] = (tmp_data[:, 2] - scale_mean[0]) / scale_std[0]
    tmp_data[:, 3] = (tmp_data[:, 3] - scale_mean[1]) / scale_std[1]
    tmp_data[:, 4] = (tmp_data[:, 4] - scale_mean[2]) / scale_std[2]

    test_data.append(tmp_data)

# ML approach
# Inspired by http://scikit-learn.org/stable/auto_examples/ensemble/plot_gradient_boosting_regression.html#sphx-glr-auto-examples-ensemble-plot-gradient-boosting-regression-py
from sklearn import ensemble
from sklearn.multioutput import MultiOutputRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_squared_error

"""
params = {'n_estimators': 1000, 'max_depth': 3, 'min_samples_split': 2,
          'learning_rate': 0.1, 'loss': 'ls'}
clf = MultiOutputRegressor(ensemble.GradientBoostingRegressor(**params))
"""

clf = KNeighborsRegressor(n_neighbors = 15)



clf.fit(X, y)
print "Training MSE: velocity / angle"
print mean_squared_error(y[:, 0], clf.predict(X)[:, 0]), mean_squared_error(y[:, 1], clf.predict(X)[:, 1])

print "Test MSE: velocity / angle"
test_error = []
for td in test_data:
    test_error.append([mean_squared_error(td[:, -2], clf.predict(td[:, 2:-2])[:, 0]),
                       mean_squared_error(td[:, -1], clf.predict(td[:, 2:-2])[:, 1])])
test_error = np.array(test_error)
print np.mean(test_error, axis=0)
print ""


if True:
    pe = []
    be = []

    import time
    t1 = time.time()

    for file_no, td in enumerate(test_data):
        print "starting file_no %s" % (file_no + 1)
        test_xy = td[:, 0:2]

        for test_count in range(10):

            # Test on random selection of test data
            start_int = np.random.randint(0, test_xy.shape[0] - 120)
            overall_trajectory = test_xy[start_int: start_int + 120, :]
            actual = test_xy[start_int + 60:start_int + 120, :]

            # Run
            known = test_xy[start_int:start_int + 60, :]

            start = known.shape[0] - 1
            end = known.shape[0] + 60 -1
            for i in range(start, end, 1):
                boundary_distance, last_angle, last_velocity, last_bearing, last_debug_points = get_features(known, i, N_FRAMES)

                boundary_distance = (boundary_distance - scale_mean[0]) / scale_std[0]
                last_angle = (last_angle - scale_mean[1]) / scale_std[1]
                last_velocity = (last_velocity - scale_mean[2]) / scale_std[2]

                new_predict = clf.predict([[boundary_distance, last_angle, last_velocity]])[0]

                new_x = known[i, 0] + new_predict[0] * math.cos(last_bearing + new_predict[1])
                new_y = known[i, 1] + new_predict[0] * math.sin(last_bearing + new_predict[1])

                if new_x < box_x[0] or new_x > box_x[1] or new_y < box_y[0] or new_y > box_y[1]:
                    while i < end:
                        known = np.append(known, [known[i, :]], axis=0)
                        i +=1
                    break

                else:
                    known = np.append(known, [[new_x, new_y]], axis=0)

            # Capture results
            prediction = known #[-60:-50,:]

            plot_data_with_boundaries(overall_trajectory, prediction, actual, arrow=False)

            # Save error
            pe.append(error(actual, prediction))
            be.append(error(actual, [actual[-1]] * 60))

            if (pe[-1] / be[-1]) > 1.0:
                pass
                # plot_data_with_boundaries(overall_trajectory, prediction, actual, arrow=False, filename='test_%s_%s' % (file_no + 1, test_count))
            # print "%d) prediction %d, baseline %d, ratio %1.2f" % (test_count, pe[-1], be[-1], pe[-1] / be[-1])

    t2 = time.time()

    # Recap results
    pe = np.average(pe)
    be = np.average(be)
    print ""
    print "error comp: prediction %s, baseline %s, ratio %1.2f" % (pe, be, pe / be)
    print "time taken %s" % (t2-t1)