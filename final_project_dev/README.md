# AI for Robotics Final Project: Team 13
Our submission uses two techniques:
    1. A simple kalman filter is used to filter the test data and determine the velocity and bearing for the next points
    2. A crash library is called to determine if one of the next points will crash. If the next point is a crashing point, the crashing library will return the angle to append to the current angle and the number of generations of points that should stay in the same place.

The kalman filter is implemented in finalproject.py and is a simple implementation that is self explanatory through reading the code. Since the crashing library required a lot of preprocessing, our team opted to put the contents of this library in a pickle file. Here are the files:
* crash_library.py - This is the library that finalproject.py imports to use the library and has wrapper functions.
* crash_data.p - This file is a pickle file that contains all of the data used by crash_library.py

The following steps will recreate the crash_data.p file:
1. Run `python motion_stages.py`. This program will loop through the training data and calculate the pre crash angle, post crash angle and crash statistics. It will generate a file named trajectory/training_data_crash.csv
2. Run `motion_angle_analysis.py`. This program will analyze the data within trajectory/training_data_crash.csv and create crash_data.p..