import numpy as np
import math

FRAME_COUNT_KNOWN = 120
FRAME_COUNT_PREDICT = 60


class HexBug():

	def __init__(self, data, parameters = None):
		self.data = data

	def prediction(self):
		return self.data


def test_model(model, data, parameters):

	# Number of tests based on frame size
	N = int(math.floor((len(data) - FRAME_COUNT_PREDICT) / FRAME_COUNT_KNOWN))

	xval_error = []
	# Compute Xval error 
	for i in range(N):

		# Prepare data
		index = i * FRAME_COUNT_KNOWN
		data_known = data[index : index + FRAME_COUNT_KNOWN]
		data_to_predict = data[index + FRAME_COUNT_KNOWN: index + (FRAME_COUNT_KNOWN + FRAME_COUNT_PREDICT)]

		###############
		#  RUN MODEL  #
		###############
		hex_bug = model(data = data_known, parameters = parameters)
		prediction = hex_bug.prediction()


		# Record error
		xval_error.append(error(prediction, data_to_predict))

	return sum(xval_error) / len(xval_error)


def error(l1, l2):
    return sum((c - a)**2 + (d - b)**2 for ((a, b), (c, d)) in zip(l1, l2))**0.5


if __name__ == "__main__":

    # Load training data
	f = open('data/training_data.txt', 'r')
	data = []
	for line in f.readlines():
		x,y = line.strip().split(',')
		data.append((int(x), int(y)))

	# Test the model
	error = test_model(HexBug, data, None)

	print error