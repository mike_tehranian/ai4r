# Example of UKF
# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes

# from __future__ import (absolute_import, division, print_function,
#                         unicode_literals)

import math
import random
import numpy as np
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.kalman import MerweScaledSigmaPoints

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
from filterpy.common import Q_discrete_white_noise
import matplotlib.pyplot as plt

def hx(x):
    """ the measurement function converts the filter's prior state x into a
    measurement vector of shape (dim_z). """
    return x[0], x[1]


def fx(state, dt):
    """ state transition function for state [x, y, heading, turning, distance].
    dt is the time step in seconds. """
    x, y, v, b, t = state[0], state[1], state[2], state[3], state[4]
    x += (v*math.cos(b))
    y += (v*math.sin(b))
    b += t
    return np.array([x, y, v, b, t])


# REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# points = MerweScaledSigmaPoints(n=2, alpha=.3, beta=2., kappa=.1)
# points = MerweScaledSigmaPoints(n=4, alpha=.1, beta=2., kappa=-1.)
dt = 1.
meas = []
b = math.pi/4.
t = .5
v = 1.
meas.append([1, 1])
for i in range(1,100):
    # if i%2 == 0:
    #     t *= -1.
    x, y = meas[len(meas)-1][0], meas[len(meas)-1][1]
    meas.append([x + v*math.cos(b) , y + v*math.sin(b)])
    b += t


# REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
points = MerweScaledSigmaPoints(n=5, alpha=.1, beta=2., kappa=1.)
ukf = UKF(dim_x=5, dim_z=2, dt=dt, hx=hx, fx=fx, points=points)
# # initial state: x, x', y, y'
ukf.x = np.array([meas[0][0], meas[0][1], 1., math.pi/4., 0.5])
ukf.P = np.diag([1., 1., 1., 1., 1.])
ukf.R = np.diag([1., 1.])

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# ukf.Q[0:2, 0:2] = Q_discrete_white_noise(2, dt=1, var=0.02)
# ukf.Q[2:4, 2:4] = Q_discrete_white_noise(2, dt=1, var=0.02)

filter_state = []

meas = np.array(meas)

filter_state2 = []
for m in meas:
    ukf.predict()
    ukf.update(m)
    x = [ukf.x[0], ukf.x[1]]
    filter_state2.append(ukf.x.copy())
    print 'fs2', x
filter_state2 = np.array(filter_state2)


for i in range(60):
    ukf.predict()
    filter_state.append(ukf.x.copy())
    x = [ukf.x[0], ukf.x[1]]
    ukf.update(x)
filter_state = np.array(filter_state)


plt.plot(meas[:, 0], meas[:, 1], 'ro')
plt.plot(filter_state[:, 0], filter_state[:, 1], 'go')
plt.plot(filter_state2[:, 0], filter_state2[:, 1])
plt.plot(meas[-1:, 0], meas[-1:, 1], 'ko')


plt.show()
