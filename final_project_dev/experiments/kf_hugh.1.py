# Example of UKF
# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes

import math
import random
import numpy as np
import numpy.linalg as la
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.kalman import MerweScaledSigmaPoints


# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
from filterpy.common import Q_discrete_white_noise
import matplotlib.pyplot as plt


def hx(x):
    return x[0], x[1], x[2], x[3]


def fx(state, dt):
    print 'state', state
    x, y, v, b, t  = state[0], state[1], state[2], state[3], state[4]
    b += t
    x = x + v*math.cos(b)
    y = y + v*math.sin(b)

    print 'x, y, v, b, t', x, y, v, b, t
    return np.array([x, y, v, b, t])


# REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
dt = 1.
meas = []
# x = x + v*math.cos(b)
# y = y - v*math.sin(b)
b = 0.
t = .3

meas.append([20,20, 0, 0])
for i in range(1,40):
    # REFERENCE: np.random.uniform(.9,1.1) taken from robot.py in project 1
    # meas.append([((i-1)+math.cos(b))*np.random.uniform(.9,1.1), 
    #         ((i-1) + math.sin(b))*np.random.uniform(.9,1.1), 1., 0])
    x, y = meas[len(meas)-1][0], meas[len(meas)-1][1]
    b+=t
    meas.append([x+math.cos(b), 
            y + math.sin(b), 1., b])

    # print b

meas = np.array(meas)
m2 = meas[:]
# REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
points = MerweScaledSigmaPoints(n=5, alpha=.1, beta=2., kappa=1.)
ukf = UKF(dim_x=5, dim_z=4, dt=dt, hx=hx, fx=fx, points=points)
# # initial state: x, x', y, y'
ukf.x = np.array([x, y, 1., 0., 0.])
ukf.P = np.diag([1., 1., 1., 1., 1.])
ukf.R = np.diag([1., 1., 1., 1.])

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# ukf.Q[0:2, 0:2] = Q_discrete_white_noise(2, dt=1, var=0.02)
# ukf.Q[2:4, 2:4] = Q_discrete_white_noise(2, dt=1, var=0.02)

filter_state = []

# for z in meas:
#     ukf.predict()
#     ukf.update(z)
#     filter_state.append(ukf.x.copy())
# filter_state = np.array(filter_state)
# ukf.batch_filter(meas[0:-1,:])
filter_state2 = []
for m in meas:
    ukf.predict()
    ukf.update(m)
    filter_state2.append(ukf.x.copy())
    x = ukf.x[0:4].copy()
filter_state2 = np.array(filter_state2)

# x = meas[-,:]
# for i in range(40):
#     ukf.predict()
#     ukf.update(x)
#     filter_state.append(ukf.x.copy())
#     x = ukf.x[0:4].copy()

# filter_state = np.array(filter_state)

# # REFERENCE: https://stackoverflow.com/questions/1349230/matplotlib-coord-sys-origin-to-top-left
# plt.gca().invert_yaxis()

plt.minorticks_on()

plt.plot(filter_state2[:, 0], filter_state2[:, 1])
m2 = np.array(m2)
plt.plot(m2[:,0], m2[:,1])
# plt.plot(filter_state[0:1, 0], filter_state[0:1, 1], 'ro')
# plt.plot(filter_state[:, 0], filter_state[:, 1], 'ro')
plt.show()

