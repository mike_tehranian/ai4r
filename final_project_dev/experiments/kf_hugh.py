# Example of UKF
# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes

# from __future__ import (absolute_import, division, print_function,
#                         unicode_literals)

import math
import random
import numpy as np
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.kalman import MerweScaledSigmaPoints

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
from filterpy.common import Q_discrete_white_noise
import matplotlib.pyplot as plt

def hx(x):
    """ the measurement function converts the filter's prior state x into a
    measurement vector of shape (dim_z). """
    print 'x[0], x[2]', x[0], x[2]
    return x[0], x[2]


def fx(state, dt):
    """ state transition function for state [x, y, heading, turning, distance].
    dt is the time step in seconds. """
    x, xp, y, yp = state[0], state[1], state[2], state[3]

    x += xp
    y += yp
    print 'x,y', x,y
    print '[x, xp, y, yp]', [x, xp, y, yp]

    return np.array([x, xp, y, yp])


# REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# points = MerweScaledSigmaPoints(n=2, alpha=.3, beta=2., kappa=.1)
# points = MerweScaledSigmaPoints(n=4, alpha=.1, beta=2., kappa=-1.)
dt = 1.
meas = []
for i in range(1,11):
    meas.append([i*np.random.uniform(.75,1.25), i*np.random.uniform(.75,1.25)])

# REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
points = MerweScaledSigmaPoints(n=4, alpha=.1, beta=2., kappa=1.)
ukf = UKF(dim_x=4, dim_z=2, dt=dt, hx=hx, fx=fx, points=points)
# # initial state: x, x', y, y'
ukf.x = np.array([0., 1., 0., 1.])
ukf.P = np.diag([1., 1., 1., 1.])
ukf.R = np.diag([1., 1.])

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
ukf.Q[0:2, 0:2] = Q_discrete_white_noise(2, dt=1, var=0.02)
ukf.Q[2:4, 2:4] = Q_discrete_white_noise(2, dt=1, var=0.02)

filter_state = []

meas = np.array(meas)
ukf.batch_filter(meas)

x = meas[-1,:]
for i in range(10):
    ukf.update(x)
    ukf.predict()
    filter_state.append(ukf.x.copy())
    x = [ukf.x[0], ukf.x[2]]

filter_state = np.array(filter_state)

# for z in meas:
#     ukf.predict()
#     ukf.update(z)
#     filter_state.append(ukf.x.copy())
# filter_state = np.array(filter_state)

# plt.plot(filter_state[:, 2], filter_state[:, 2])
plt.plot(meas[:, 0], meas[:, 1], 'ro')
plt.plot(filter_state[:, 0], filter_state[:, 2], 'go')
plt.show()
# print('UKF standard deviation {:.3f} meters'.format(np.std(filter_state - measurements)))
