# Example of UKF
# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes

import math
import random
import numpy as np
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.kalman import MerweScaledSigmaPoints

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
from filterpy.common import Q_discrete_white_noise
import matplotlib.pyplot as plt

# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes
def hx(x):
    return x[0], x[1], x[2], x[3]


# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes
def fx(state, dt):
    x, y, v, b, t, a  = state[0], state[1], state[2], state[3], state[4], state[5]
    # x1 = x0 + velocity*math.cos(bearing)
    # y1 = y0 - velocity*math.sin(bearing)

    b += t
    v = v + v*a
    x = x + v*math.cos(b)
    y = y - v*math.sin(b)

    # print 'x,y', x,y
    # print '[x, xp, y, yp]', [x, y, v, b, t]

    return np.array([x, y, v, b, t, a])


# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes
dt = 1.
meas = []
# x = x + v*math.cos(b)
# y = y - v*math.sin(b)
b = 0.
t = .9
v=1.
a=.1
for i in range(1,21):
    # REFERENCE: np.random.uniform(.9,1.1) taken from robot.py in project 1
    meas.append([((i-1)+math.cos(b))*np.random.uniform(.9,1.1), 
            ((i-1) + math.sin(b))*np.random.uniform(.9,1.1), 
            v*np.random.uniform(.9,1.1), 
            b*np.random.uniform(.9,1.1)])
    # meas.append([((i-1)+v*math.cos(b)), 
    #         ((i-1) + v*math.sin(b)), v, b])

    b+=t
    v = v+(v*a)
    # print b

m2 = meas[:]
# REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
points = MerweScaledSigmaPoints(n=6, alpha=.1, beta=2., kappa=1.)
ukf = UKF(dim_x=6, dim_z=4, dt=dt, hx=hx, fx=fx, points=points)
# # initial state: x, x', y, y'
ukf.x = np.array([1., 1., .5, 0.7, 0., 0.])
ukf.P = np.diag([1., 1., 1., 1., 1., 1.])
ukf.R = np.diag([1., 1., 1., 1.])

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
ukf.Q[0:3, 0:3] = Q_discrete_white_noise(3, dt=1, var=2)
ukf.Q[3:6, 3:6] = Q_discrete_white_noise(3, dt=1, var=2)

# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes
filter_state = []
# ukf.predict()
for z in meas:
    ukf.update(z)
    # filter_state.append(ukf.x.copy())
    x = ukf.x[:4].copy()
    ukf.predict()
# filter_state = np.array(filter_state)
for i in range(100):
    ukf.update(x)
    ukf.predict()
    filter_state.append(ukf.x[0:4].copy())
    x = ukf.x[:4].copy()

filter_state = np.array(filter_state)
    
f_meas = []
for i in range(20,32):
    # REFERENCE: np.random.uniform(.9,1.1) taken from robot.py in project 1
    f_meas.append([((i-1)+math.cos(b))*np.random.uniform(.9,1.1), 
            ((i-1) + math.sin(b))*np.random.uniform(.9,1.1), 
            v*np.random.uniform(.9,1.1), 
            b*np.random.uniform(.9,1.1)])
    # meas.append([((i-1)+v*math.cos(b)), 
    #         ((i-1) + v*math.sin(b)), v, b])

    b+=t
    v = v+(v*a)
    # print b
f_meas = np.array(f_meas)

# REFERENCE: https://stackoverflow.com/questions/1349230/matplotlib-coord-sys-origin-to-top-left
plt.gca().invert_yaxis()

plt.minorticks_on()

m2 = np.array(m2)
plt.plot(m2[:,0], m2[:,1])
plt.plot(filter_state[:, 0], filter_state[:, 1])
plt.plot(f_meas[:,0], f_meas[:,1])

plt.show()
# print('UKF standard deviation {:.3f} meters'.format(np.std(filter_state - measurements)))
