import numpy as np
import math
from motion_utils import plot_prepost_data_with_boundaries, eucledian_distance, truncate_angle
from motion_variables import Boundary

##################################################################
# Estimates stages of motion (motion -> crash -> motion -> ...)  #
##################################################################


from motion_variables import *
ANALYSIS_START_BUFFER = 5 # How many frames earlier to start the linear fit from

def bearing_estimate(data, index):
    # Get bearing based on last ANALYSIS_START_BUFFER positions, if possible
    analysis_start = (index - ANALYSIS_START_BUFFER) if (index - ANALYSIS_START_BUFFER) >= 0 else 0
    coefs = np.polyfit(data[analysis_start:index, 0], data[analysis_start:index, 1], 1)

    return math.atan2(np.polyval(coefs,data[index, 0]) - np.polyval(coefs,data[analysis_start, 0]),
                      data[index, 0] - data[analysis_start, 0])

def within_preliminary_range(xy, bearing, verbose=False):
    prelim_range = 15

    def angle_in_range(t):
        angle_range = 3.0 / 4
        if angle_range * np.pi / 2 > t > - angle_range * np.pi / 2:
            return True
        else:
            return False

    # Compare to x walls
    if xy[0] <= box_x[0] + prelim_range + abs(prelim_range * math.cos(bearing)) and angle_in_range(bearing-np.pi):
        if verbose:
            print "getting close to x wall at position %s %s with range %s" % (xy[0], xy[1], prelim_range + abs(prelim_range * math.cos(bearing)))
            print "bearing: %s" % bearing
        return Boundary.X_LEFT

    if xy[0] >= box_x[1] - prelim_range - abs(prelim_range * math.cos(bearing)) and angle_in_range(bearing):
        if verbose:
            print "getting close to x wall at position %s %s with range %s" % (xy[0], xy[1], prelim_range + abs(prelim_range * math.cos(bearing)))
            print "bearing: %s" % bearing
        return Boundary.X_RIGHT

    # Compare to y walls
    if xy[1] <= box_y[0] + prelim_range + abs(prelim_range * math.sin(bearing)) and angle_in_range(bearing+np.pi/2):
        if verbose:
            print "getting close to y wall at position %s %s with range %s" % (xy[0], xy[1], prelim_range + abs(prelim_range * math.sin(bearing)))
            print "bearing: %s" % bearing
        return Boundary.Y_BOTTOM

    if xy[1] >= box_y[1] - prelim_range - abs(prelim_range * math.sin(bearing)) and angle_in_range(bearing-np.pi/2):
        if verbose:
            print "getting close to y wall at position %s %s with range %s" % (xy[0], xy[1], prelim_range + abs(prelim_range * math.sin(bearing)))
            print "bearing: %s" % bearing
        return Boundary.Y_TOP

    # Compare to candle
    if eucledian_distance(candle_xy, xy) <= (candle_r + 2 * prelim_range):
        theta_xy_candle = truncate_angle(bearing - math.atan2(candle_xy[1] - xy[1], candle_xy[0] - xy[0]))

        # Check that angle is within bounds, and add distance buffer based on incident angle
        if angle_in_range(theta_xy_candle) and \
                eucledian_distance(candle_xy, xy) <= (candle_r + prelim_range + abs(prelim_range * math.cos(theta_xy_candle))):

            if verbose:
                print "theta_xy_candle %s" % theta_xy_candle
                print "getting close to candle at position %s %s with range %s" % (xy[0], xy[1], prelim_range + abs(prelim_range * math.cos(theta_xy_candle)))
            return Boundary.CANDLE

    return False


def find_next_path(data, start_index = None, verbose=False):
    # Start analysing data once the bug has started moving a bit
    if start_index:
        index_range = start_index
    else:
        index_range = 1

    while eucledian_distance(data[0, :], data[index_range, :]) < 30:
        index_range += 1

    # Find index where the hexbug is within preliminary crash range
    boundary = False
    while not boundary:
        index_range += 1
        boundary = within_preliminary_range(data[index_range, :], bearing_estimate(data, index_range), verbose)

    # index_range += 1  # index_range is point that failed the test

    # Find best straight line fit, and generate new position, based on last 5 positions
    extra_points = 5
    analysis_start = (index_range - ANALYSIS_START_BUFFER) if (index_range - ANALYSIS_START_BUFFER) >= 0 else 0
    fit_order = 1

    # Decide wether to fit to x or y based on impact angle
    impact_angle = bearing_estimate(data, index_range)
    if (impact_angle > np.pi / 4 and impact_angle < 3 * np.pi / 4) or \
            (impact_angle < - np.pi / 4 and impact_angle > - 3 * np.pi / 4):

        coefs = np.polyfit(data[analysis_start:index_range, 1], data[analysis_start:index_range, 0], fit_order)
        new_x = np.array(
            np.polyval(coefs, data[0:index_range + extra_points, 1]))  # Generate x values 10 points past index_range
        new_data = np.transpose(np.vstack((new_x, data[0:index_range + extra_points, 1])))

    else:

        coefs = np.polyfit(data[analysis_start:index_range, 0], data[analysis_start:index_range, 1], fit_order)
        new_y = np.array(
            np.polyval(coefs, data[0:index_range + extra_points, 0]))  # Generate y values 10 points past index_range
        new_data = np.transpose(np.vstack((data[0:index_range + extra_points, 0], new_y)))

    # Find velocity minimum
    distance = []
    distance_minimum_index = None
    for i in range(new_data.shape[0] - 1):
        distance.append(eucledian_distance(new_data[i, :], new_data[i + 1, :]))

        if i > (index_range-1) and \
                (distance[-1] <= 2.0 or distance[-2] == 0 or (distance[-1] / distance[-2]) <= 0.5):  # Check when speed is minimum & at least 5 steps from range flag
            distance_minimum_index = len(distance)  # Get point closest to the crash
            break

    if verbose:
        s = ''
        for d in distance[analysis_start:]:
            s += " %2.1f |" % d
        print s

    if distance_minimum_index is None:
        # Failed to find a suitable minimum, so was probably close to boundary but did not crash
        # Run recursively
        distance_minimum_index, pre_crash, post_crash, crash_stats = find_next_path(data, start_index = new_data.shape[0], verbose=verbose)

    else:
        # Get velocity from average of last 5 frames
        velocity = np.mean(distance[-7:-2])

        # Keep data from 5 frames before crash
        pre_crash = new_data[distance_minimum_index-5:distance_minimum_index, :]

        # Find index to move by after crash
        post_crash_time_within_d10 = 1
        post_crash_index = 1

        d = eucledian_distance(data[distance_minimum_index, :], data[distance_minimum_index + post_crash_index, :])
        while d < 50:
            if d < 10:
                post_crash_time_within_d10 += 1
            d = eucledian_distance(data[distance_minimum_index, :], data[distance_minimum_index + post_crash_index, :])
            post_crash_index += 1

        # Evaluate direction after crash
        if abs(data[distance_minimum_index + post_crash_index, 1] - data[distance_minimum_index - 1, 1]) > \
                abs(data[distance_minimum_index + post_crash_index, 0] - data[distance_minimum_index - 1, 0]):

            coefs = np.polyfit(data[distance_minimum_index - 1:distance_minimum_index + post_crash_index, 1],
                               data[distance_minimum_index - 1:distance_minimum_index + post_crash_index, 0], fit_order)
            new_y = np.arange(data[distance_minimum_index - 1, 1],
                              data[distance_minimum_index + post_crash_index, 1],
                              (data[distance_minimum_index + post_crash_index, 1] - data[
                                  distance_minimum_index - 1, 1]) / 5)
            new_x = np.array(np.polyval(coefs, new_y))
            post_crash = np.transpose(np.vstack((new_x, new_y)))

        else:
            coefs = np.polyfit(data[distance_minimum_index - 1:distance_minimum_index + post_crash_index, 0],
                               data[distance_minimum_index - 1:distance_minimum_index + post_crash_index, 1], fit_order)
            new_x = np.arange(data[distance_minimum_index - 1, 0],
                              data[distance_minimum_index + post_crash_index, 0],
                              (data[distance_minimum_index + post_crash_index, 0] - data[distance_minimum_index - 1, 0]) / 5)
            new_y = np.array(np.polyval(coefs, new_x))
            post_crash = np.transpose(np.vstack((new_x, new_y)))

        # Gather statistics: reflect angle
        reflected_angle = math.atan2(post_crash[-1, 1] - post_crash[0, 1],
                                     post_crash[-1, 0] - post_crash[0, 0])


        reflected_angle -= impact_angle

        # Gather statistics: distance from boundary
        distance_crash = None
        if boundary == Boundary.X_LEFT:
            distance_crash = data[distance_minimum_index, 0] - box_x[0]
            impact_angle = (impact_angle - np.pi)

        elif boundary == Boundary.X_RIGHT:
            distance_crash = box_x[1] - data[distance_minimum_index, 0]

        elif boundary == Boundary.Y_BOTTOM:
            distance_crash = data[distance_minimum_index, 1] - box_y[0]

            impact_angle = (impact_angle + np.pi / 2)

        elif boundary == Boundary.Y_TOP:
            distance_crash = box_y[1] - data[distance_minimum_index, 1]

            impact_angle = (impact_angle - np.pi / 2)

        elif boundary == Boundary.CANDLE:
            distance_crash = eucledian_distance(data[distance_minimum_index, :], candle_xy)

            impact_angle = truncate_angle(impact_angle - math.atan2(candle_xy[1] - data[distance_minimum_index,1],candle_xy[0] - data[distance_minimum_index,0]))

        # Stores relative_index, x, y, boundary, distance_crash, impact_angle, reflected_angle, velocity, post_crash_time_within_d10
        crash_stats = [distance_minimum_index,
                       data[distance_minimum_index,0], data[distance_minimum_index,1],
                       boundary, distance_crash, impact_angle, reflected_angle, velocity, post_crash_time_within_d10]

    return distance_minimum_index, pre_crash, post_crash, crash_stats


#####################
# MAIN SCRIPT START #
#####################

verbose = True

# delete previous images
import os

filelist = [ f for f in os.listdir("trajectory") if f.endswith(".png") ]
for f in filelist:
    os.remove("trajectory/" + f)


# Load data
filename = 'test01'
f = open('inputs/' + filename + '.txt', 'r')
data = []
for line in f.readlines():
    x, y = line.strip().split(',')
    data.append((int(x), int(y)))
data = np.array(data)
f.close()

import csv
f = open('trajectory/' + filename + '_crash.csv', 'w')
writer = csv.writer(f, delimiter=',')

# plot_data_with_boundaries(data[110:117,:], data[110:117,:]) # Debug plot

crash_index = []

absolute_index = 0
# for _ in range(5):
while absolute_index < data.shape[0]:
    if verbose:
        print ""
        print "starting at index %s" % absolute_index
    else:
        print "at frame %s" % absolute_index

    try:
        index, pre_crash, post_crash, crash_stats = find_next_path(data[absolute_index:, :], start_index = None, verbose=verbose)
    except IndexError, e:
        print e
        break

    if verbose:
        # print crash stats
        print crash_stats

        # save plots
        plot_prepost_data_with_boundaries(data[absolute_index:absolute_index + index + 20,:],
                                  pre_crash,
                                  post_crash,
                                  "trajectory %s to %s" % (absolute_index, absolute_index + index),)
    absolute_index += index
    crash_stats[0] = absolute_index
    writer.writerow(crash_stats)

# Save file
f.close()