from crash_library import check_crashing, LEN_REQ
import numpy as np
from motion_utils import plot_data_with_boundaries

# Load data
filename = 'test01'
f = open('inputs/' + filename + '.txt', 'r')
data = []
for line in f.readlines():
    x, y = line.strip().split(',')
    data.append((int(x), int(y)))
data = np.array(data)
f.close()

# Test crash_library
N = 100
for i in range(0, data.shape[0] - N, N):
    crashes_for_current_range = []
    for ii in range(i + LEN_REQ, i + N, 1):
        if check_crashing(data[ii - LEN_REQ:ii, :]) is not None:
            crashes_for_current_range.append(data[ii,:])

    crashes_for_current_range = np.array(crashes_for_current_range)

    plot_data_with_boundaries(data[i:i+N, :], crashes_for_current_range,
                              filename="crash_library_test_%s_%s" % (filename, i), arrow=False)