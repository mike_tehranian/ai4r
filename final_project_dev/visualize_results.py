#############################################################
# Use this file to visual the results of a test run
# Actuals are in green. Predictions are in red. The first
# two points of the training data are in black.
#############################################################


# REFENCE: Used API reference at https://matplotlib.org
import matplotlib.pyplot as plt
import numpy as np

test = 1

# REFERENCE: https://stackoverflow.com/questions/1349230/matplotlib-coord-sys-origin-to-top-left
# plt.gca().invert_yaxis()

# plt.yticks(range(0,10))
# plt.xticks(range(0,10))
plt.minorticks_on()

# REFERENCE: Used np.loadtxt based on post located at
#  https://stackoverflow.com/questions/11023411/how-to-import-csv-data-file-into-scikit-learn
# actuals  = np.loadtxt('actual/gm_actual02_noise.txt', delimiter=',') # load file
actuals  = np.loadtxt('actual/%02d.txt' % test, delimiter=',') # load file
predictions  = np.loadtxt('prediction.txt', delimiter=',') # load file
# training  = np.loadtxt('inputs/gm_test02_noise.txt', delimiter=',') # load file
# training  = np.loadtxt('inputs/test%02d.txt' % test, delimiter=',') # load file
original  = np.loadtxt('inputs/test%02d.txt' % test, delimiter=',') # load file

stuff = np.array([  [ 386.92814494,  310.0050835 ],
                    [ 388.86280567,  317.88778819],
                    [ 390.77811979,  325.69166583],
                    [ 392.67428078,  333.4175047 ],
                    [ 394.58940337,  341.22060196]])

# plt.plot(actuals[0,0], actuals[0,1], 'go')
# plt.plot(predictions[0,0], predictions[0,1], 'ro')

plt.grid(True)
# Add box
box_x = (80, 566)
box_y = (34, 325)
candle_xy = (333, 178)
candle_r = 39.4

plt.plot([box_x[0], box_x[0]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Left wall
plt.plot([box_x[1], box_x[1]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Right wall
plt.plot([box_x[0], box_x[1]], [box_y[0], box_y[0]], 'k', linewidth=3.0)  # Top wall per the video
plt.plot([box_x[0], box_x[1]], [box_y[1], box_y[1]], 'k', linewidth=3.0)  # Bottom wall per the video

# Add circle
circle = plt.Circle(candle_xy, candle_r, fill=False, color='k', linewidth=3.0)
fig = plt.gcf()
ax = fig.gca()
ax.add_artist(circle)


plt.plot(original[-30:-1,0], original[-30:-1,1], 'b')
plt.plot(actuals[:,0], actuals[:,1], 'og')
plt.plot(predictions[:60,0], predictions[:60,1], 'oy')
# plt.plot(stuff[:,0], stuff[:,1], 'or')

# plt.plot(training[-2,0], training[-2,1], 'ko')


plt.show()