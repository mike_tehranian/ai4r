from motion_variables import *
import matplotlib.pyplot as plt
import numpy as np
import math

def truncate_angle(t): # from robot.py of Project 2
    return ((t + np.pi) % (2 * np.pi)) - np.pi


def eucledian_distance(loc1, loc2):
    return math.sqrt((loc1[0] - loc2[0]) ** 2 + (loc1[1] - loc2[1]) ** 2)

# Plot trajectory, and bounding boxes
def plot_data_with_boundaries(data, trajectory, filename=None, arrow = True):

    plt.grid(True)
    # Add box
    plt.plot([box_x[0], box_x[0]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Left wall
    plt.plot([box_x[1], box_x[1]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Right wall
    plt.plot([box_x[0], box_x[1]], [box_y[0], box_y[0]], 'k', linewidth=3.0)  # Top wall per the video
    plt.plot([box_x[0], box_x[1]], [box_y[1], box_y[1]], 'k', linewidth=3.0)  # Bottom wall per the video

    # Show trajectory
    plt.plot(data[:, 0], data[:, 1], 'b')
    if len(trajectory) > 0:
        plt.plot(trajectory[:, 0], trajectory[:, 1], 'or')

    # Add circle
    circle = plt.Circle(candle_xy, candle_r, fill=False, color='k', linewidth=3.0)
    fig = plt.gcf()
    ax = fig.gca()
    ax.add_artist(circle)

    # Add arrow
    if arrow == True:
        ax.arrow(trajectory[0, 0], trajectory[0, 1],
                 trajectory[-1, 0] - trajectory[0, 0], trajectory[-1, 1] - trajectory[0, 1],
                 width = 0.5, fill = False)

    if filename is None:
        plt.show()
    else:
        plt.savefig('trajectory/' + filename)

    plt.close()

# Plot trajectory, pre & post crash, and bounding boxes
def plot_prepost_data_with_boundaries(data, pre_crash, post_crash, filename=None, arrow = True):
    plt.grid(True)

    # Add box
    plt.plot([box_x[0], box_x[0]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Left wall
    plt.plot([box_x[1], box_x[1]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Right wall
    plt.plot([box_x[0], box_x[1]], [box_y[0], box_y[0]], 'k', linewidth=3.0)  # Top wall per the video
    plt.plot([box_x[0], box_x[1]], [box_y[1], box_y[1]], 'k', linewidth=3.0)  # Bottom wall per the video

    # Show trajectory
    plt.plot(data[:, 0], data[:, 1], 'b')
    plt.plot(pre_crash[:, 0], pre_crash[:, 1], 'og')
    plt.plot(post_crash[:, 0], post_crash[:, 1], 'or')

    # Add circle
    circle = plt.Circle(candle_xy, candle_r, fill=False, color='k', linewidth=3.0)
    fig = plt.gcf()
    ax = fig.gca()
    ax.add_artist(circle)

    if filename is None:
        plt.show()
    else:
        plt.savefig('trajectory/' + filename)

    plt.close()
