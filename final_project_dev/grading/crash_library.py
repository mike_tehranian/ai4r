import numpy as np
import math
import pickle

from motion_variables import *

# LOAD PICKLED DATA ONCE
crash_data = pickle.load(open( "crash_data.p", "rb"))
LEN_REQ = 5

# Knn data
knn_wait_time = np.array(crash_data['knn_wait_time'])

def knn_search(xy, knn_wait_time):
    knn_distance = [None] * knn_wait_time.shape[0]
    for i in range(knn_wait_time.shape[0]):
        knn_distance[i] = eucledian_distance(xy, knn_wait_time[i,0:2])
    knn_distance = np.array(knn_distance)

    index = np.argsort(knn_distance)
    k = 10
    return np.median(knn_wait_time[index[0:k],2])

# Help functions
def truncate_angle(t): # from robot.py of Project 2
    return ((t + np.pi) % (2 * np.pi)) - np.pi


def eucledian_distance(loc1, loc2):
    return math.sqrt((loc1[0] - loc2[0]) ** 2 + (loc1[1] - loc2[1]) ** 2)

def check_crashing(array_points):
    ###################################################################
    # INPUT:
    # Takes array_points as last positions
    # Expects at least 5 points
    #
    # OUTPUT:
    # Returns None if not crashed
    # Returns [wait_time, added_angle] if crashed
    #   wait_time is number of frames to wait at crashed location for
    #   added_angle is angle to be added to current bearing
    ###################################################################

    # Check input
    if len(array_points) < LEN_REQ:
        raise Exception('array_points should be at least 5 points in length')

    # Current location
    xy = array_points[-1,:]

    # Assess wether within plausible range of boundaries
    boundary = None
    prelim_range = max(crash_data['distances_median'])

    if xy[0] <= box_x[0] + prelim_range:
        boundary = Boundary.X_LEFT

    elif xy[0] >= box_x[1] - prelim_range:
        boundary = Boundary.X_RIGHT

    elif xy[1] <= box_y[0] + prelim_range:
        boundary = Boundary.Y_BOTTOM

    elif xy[1] >= box_y[1] - prelim_range:
        boundary = Boundary.Y_TOP

    elif eucledian_distance(candle_xy, xy) <= (candle_r + prelim_range):
        boundary = Boundary.CANDLE
    else:
        return None

    # Get current bearing
    if abs(np.min(array_points[-LEN_REQ:, 1]) - np.max(array_points[-LEN_REQ:, 1])) > \
            abs(np.min(array_points[-LEN_REQ:, 0]) - np.max(array_points[-LEN_REQ:, 0])):
        # Assume direction is along y axis
        coefs = np.polyfit(array_points[-LEN_REQ:, 1], array_points[-LEN_REQ:, 0], 1)
        new_y = [array_points[-LEN_REQ, 1], array_points[-1, 1]]
        new_x = np.polyval(coefs, new_y)

        bearing = math.atan2(new_y[1] - new_y[0], new_x[1] - new_x[0])

    else:
        # Assume direction is along x axis
        coefs = np.polyfit(array_points[-LEN_REQ:, 0], array_points[-LEN_REQ:, 1], 1)
        new_x = [array_points[-LEN_REQ, 0], array_points[-1, 0]]
        new_y = np.polyval(coefs, new_x)

        bearing = math.atan2(new_y[1] - new_y[0], new_x[1] - new_x[0])

    # Confirm crash by accounting for angle
    angles = np.array(crash_data['angles'])
    crashed = False

    if boundary == Boundary.X_LEFT:
        angle = truncate_angle(bearing - np.pi)
        if abs(angle) <= crash_data['max_angle'] and \
                        xy[0] <= box_x[0] + crash_data['distances_median'][(np.abs(angles - angle)).argmin()]:
            crashed = True

    elif boundary == Boundary.X_RIGHT:
        angle = bearing
        if abs(angle) <= crash_data['max_angle'] and \
                        xy[0] >= box_x[1] - crash_data['distances_median'][(np.abs(angles - angle)).argmin()]:
            crashed = True

    elif boundary == Boundary.Y_BOTTOM:
        angle = truncate_angle(bearing + np.pi / 2)
        if abs(angle) <= crash_data['max_angle'] and \
                        xy[1] <= box_y[0] + crash_data['distances_median'][(np.abs(angles - angle)).argmin()]:
            crashed = True

    elif boundary == Boundary.Y_TOP:
        angle = truncate_angle(bearing - np.pi / 2)
        if abs(angle) <= crash_data['max_angle'] and \
                        xy[1] >= box_y[1] - crash_data['distances_median'][(np.abs(angles - angle)).argmin()]:
            crashed = True

    elif boundary == Boundary.CANDLE:
        angle = truncate_angle(bearing - math.atan2(candle_xy[1] - xy[1], candle_xy[0] - xy[0]))
        if abs(angle) <= crash_data['max_angle'] and \
                        eucledian_distance(candle_xy, xy) <= (candle_r + crash_data['distances_median'][(np.abs(angles - angle)).argmin()]):
            crashed = True

    if not crashed:
        return None

    # Provide crash characteristics
    wait_time = knn_search(xy, knn_wait_time)

    if boundary != Boundary.CANDLE:
        added_angle = crash_data['angles_median_box'][(np.abs(angles - angle)).argmin()]
    elif boundary == Boundary.CANDLE:
        added_angle = crash_data['angles_median_candle'][(np.abs(angles - angle)).argmin()]

    return (wait_time, added_angle)