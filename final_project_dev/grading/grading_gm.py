# Testing script to make sure your finalproject.py operates correctly with
# the grading environment.
#
#   Place grading.py in the directory with finalproject.py
#   Create an actual directory above the directory containing finalproject.py
#       Place any files to compare your predictions to in ..\actual
#       and name them grade01.txt, grade02.txt....
#
#   basedirectory/
#       actual/
#           grade01.txt
#           grade02.txt
#           .
#           .
#           .
#       finalproject/
#           finalproject.py
#           grading.py
#           prediction.txt
#           inputs/
#               test01.txt
#               test02.txt
#               .
#               .
#               .
#   
#   
#   Use the following (or similar) in your finalproject.py to open the input file
#   passed on the command line:
#           input_file = open(sys.argv[1], 'r')
#
#   Use the following (or similar) in your finalproject.py to open the prediction.txt file
#           output_file = open('prediction.txt','w+')
#
#   To call this grading script execute the following from a command line:
#       python grading.py  


import os, sys

NUM_INPUTS = 10

def error(l1, l2):
    return sum((c - a)**2 + (d - b)**2 for ((a, b), (c, d)) in zip(l1, l2))**0.5

def convert_line(line):
    x, y = line.split(',')
    return int(x.strip()), int(y.strip())

inputs = ["./inputs/test%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
actuals = ["./actual/%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
tests = []
tests_baseline = []
for i, filename in enumerate(inputs):
    os.system('python finalproject.py %s' % (filename))

    # last_position = []
    for line in open(filename, 'r').readlines():
        last_position = convert_line(line)
    tests.append(([convert_line(line) for line in open('prediction.txt', 'r').readlines()],
                  [convert_line(line) for line in open(actuals[i], 'r').readlines()]))

    for line in open(actuals[i], 'r').readlines():
        last_position = convert_line(line)

    tests_baseline.append(([last_position] * 60, \
        [convert_line(line) for line in open(actuals[i], 'r').readlines()]))

    # print tests
errors = sorted([error(l1, l2) for (l1, l2) in tests])
errors_baseline = sorted([error(l1, l2) for (l1, l2) in tests_baseline])

print 'total error for prediction:', sum(errors[1:-1]) / (NUM_INPUTS - 2)

print 'baseline error:', sum(errors_baseline[1:-1]) / (NUM_INPUTS - 2)
print 'error/baseline:', (sum(errors[1:-1]) / (NUM_INPUTS - 2)) / (sum(errors_baseline[1:-1]) / (NUM_INPUTS - 2))

