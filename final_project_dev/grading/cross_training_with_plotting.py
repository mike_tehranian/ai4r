# Testing script to make sure your finalproject.py operates correctly with
# the grading environment.
#
#   Place grading.py in the directory with finalproject.py
#   Create an actual directory above the directory containing finalproject.py
#       Place any files to compare your predictions to in ..\actual
#       and name them grade01.txt, grade02.txt....
#
#   basedirectory/
#       actual/
#           grade01.txt
#           grade02.txt
#           .
#           .
#           .
#       finalproject/
#           finalproject.py
#           grading.py
#           prediction.txt
#           inputs/
#               test01.txt
#               test02.txt
#               .
#               .
#               .
#   
#   
#   Use the following (or similar) in your finalproject.py to open the input file
#   passed on the command line:
#           input_file = open(sys.argv[1], 'r')
#
#   Use the following (or similar) in your finalproject.py to open the prediction.txt file
#           output_file = open('prediction.txt','w+')
#
#   To call this grading script execute the following from a command line:
#       python grading.py

import matplotlib.pyplot as plt

box_x = (80, 566)
box_y = (34, 325)
candle_xy = (333, 178)
candle_r = 39.4

def plot_actual_predictions(train, actual, prediction, filename=None):

    plt.grid(True)

    # Show trajectory
    plt.plot(train[:, 0], train[:, 1], 'b')
    plt.plot(actual[:, 0], actual[:, 1], 'og')
    plt.plot(prediction[:, 0], prediction[:, 1], 'oy')

    # Add box
    plt.plot([box_x[0], box_x[0]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Left wall
    plt.plot([box_x[1], box_x[1]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Right wall
    plt.plot([box_x[0], box_x[1]], [box_y[0], box_y[0]], 'k', linewidth=3.0)  # Top wall per the video
    plt.plot([box_x[0], box_x[1]], [box_y[1], box_y[1]], 'k', linewidth=3.0)  # Bottom wall per the video

    # Add circle
    circle = plt.Circle(candle_xy, candle_r, fill=False, color='k', linewidth=3.0)
    fig = plt.gcf()
    ax = fig.gca()
    ax.add_artist(circle)


    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)

    plt.close()


import os, sys
import numpy as np

NUM_INPUTS = 10

def error(l1, l2):
    return sum((c - a)**2 + (d - b)**2 for ((a, b), (c, d)) in zip(l1, l2))**0.5

def convert_line(line):
    x, y = line.split(',')
    return int(x.strip()), int(y.strip())

# inputs = ["./inputs/test%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
# actuals = ["../actual/%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
for z in range(1):
    tests = []
    for i in range(10):
        r = np.random.randint(0,35999-1800)
        f = open('data/training_data.txt')
        lc = 0
        # print r
        while lc < r:
            lc+=1
            f.readline()
        lc = 0
        train_filename = './train_data_tmp.txt'
        train_file = open(train_filename, 'w+')
        while lc < 1740:
            l = f.readline()
            train_file.write(l)
            lc += 1
        train_file.close()
        actual_filename = './actual_data_tmp.txt'
        actual_file = open(actual_filename, 'w+')
        lc = 0
        while lc < 60:
            l = f.readline()
            actual_file.write(l)
            lc += 1
        actual_file.close()
        

        os.system('python finalproject.py %s' % (train_filename))

        # plot
        prediction = np.array([convert_line(line) for line in open('prediction.txt', 'r').readlines()])
        actual = np.array([convert_line(line) for line in open(actual_filename, 'r').readlines()])
        train = np.array([convert_line(line) for line in open(train_filename, 'r').readlines()])

        plot_actual_predictions(train, actual, prediction, "cross_training_pic_%s_%s" % (z, i))

        tests.append((prediction, actual))

    errors = sorted([error(l1, l2) for (l1, l2) in tests])
    print sum(errors[1:-1]) / (NUM_INPUTS - 2)

