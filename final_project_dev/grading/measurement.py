###############################################################
# Extracts and derives measurements for a measurement file
# and returns them in an numpy array
#
# function: get_measurements
# input: filename with x and y position
# output: ndarray in following format
#   [x, y, dist, bearing, turning, acceleration]
###############################################################


import sys
import numpy as np
import math


def get_measurements(filename):
        # REFERENCE: Used np.loadtxt based on post located at
        #  https://stackoverflow.com/questions/11023411/how-to-import-csv-data-file-into-scikit-learn
        calc  = np.loadtxt(filename, delimiter=',') # load file

        # add empty cols
        calc = np.insert(calc, 2, 0, axis=1)
        calc = np.insert(calc, 2, 0, axis=1)
        calc = np.insert(calc, 2, 0, axis=1)
        calc = np.insert(calc, 2, 0, axis=1)
        calc = np.insert(calc, 2, 0, axis=1)
        calc = np.insert(calc, 2, 0, axis=1)
        calc = np.insert(calc, 2, 0, axis=1)
        calc = np.insert(calc, 2, 0, axis=1)

        # copy next row into cols 2 and 3 of current row
        calc[0:-1,2:4] = calc[1:,0:2]

        # calc distance
        calc[:,4] = np.sqrt((calc[:,2] - calc[:,0])**2 + (calc[:,3] - calc[:,1])**2)
        
        # calc bearing
        calc[:,5] = ((np.arctan2(calc[:,3] - calc[:,1], calc[:,2] - calc[:,0]) + (2.*math.pi)))%(2.*math.pi)
        
        # copy bearing from next row up to current
        calc[0:-1,6:] = calc[1:,5:6]

        # calculate turning
        calc[:,7] = calc[:,6] - calc[:,5]

        # adjust turning angle for the case where it is closer to turn 
        # the other direction
        tcalc = calc[:,7]
        tcalc_idx = 2*math.pi- np.abs(tcalc) < np.abs(tcalc)
        tcalc[tcalc_idx] = (-1.0)*(tcalc[tcalc_idx]/np.abs(tcalc[tcalc_idx]))*(2*math.pi - np.abs(tcalc[tcalc_idx]))
        calc[:,7] = tcalc

        # copy dist from next row up to current
        calc[0:-1,8] = calc[1:,4]

        # calculate acceleration
        calc[:,9] = calc[:,8]- calc[:,4]

        # print calc 
        # return the following columns
        # x, y, distance, bearing, turning, acceleration
        measurements = calc[:,[0,1,4,5,7,9]]
        measurements[-1,2:]=0
        measurements[-2,4:]=0
        
        return measurements