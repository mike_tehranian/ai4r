# You want to make sure your version produces better error rates than this :)

import sys
import measurement as m
import position as pos
import math
import numpy as np
# import sklearn.neighbors as sk

import hughukf as ukf

filename = sys.argv[1]
# x, y = open(filename, 'r').readlines()[-1].split(',')
meas = m.get_measurements(filename)

output = ukf.run_ukf(meas[:,0:4], 60)

with open('prediction.txt', 'w') as f:
    for i in range(60):
        o = output[i]
        print o
        print >> f, '{},{}'.format(int(o[0]), int(o[1]))
