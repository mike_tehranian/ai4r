# Example of UKF
# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes

import math
import random
import numpy as np
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.kalman import MerweScaledSigmaPoints
import measurement as m

# REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
from filterpy.common import Q_discrete_white_noise
import matplotlib.pyplot as plt

# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes
def hx(x):
    return x[0], x[1], x[2], x[3]


# REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# I started with the same code and the ukf.py file and made changes
def fx(state, dt):
    x, y, v, b, t, a  = state[0], state[1], state[2], state[3], state[4], state[5]
    b += t
    v = v + .5*a
    x = x + v*math.cos(b)
    y = y - v*math.sin(b)
    return np.array([x, y, v, b, t, a])

def run_ukf(meas, n_points):
    # REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
    # I started with the same code and the ukf.py file and made changes
    # REFERENCE: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
    dt = 1.
    points = MerweScaledSigmaPoints(n=6, alpha=.1, beta=2., kappa=1.)
    ukf = UKF(dim_x=6, dim_z=4, dt=dt, hx=hx, fx=fx, points=points)
    # # initial state: x, x', y, y'
    ukf.x = np.array([meas[0,0], meas[0,1], meas[0,2], meas[0,3], 0., 0.])
    ukf.P = np.diag([100., 100., 100., 100., 100., 100.])
    ukf.R = np.diag([100., 100., 100., 100.])

    # REFERENCE: Taken from http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
    ukf.Q[0:3, 0:3] = Q_discrete_white_noise(3, dt=1, var=2)
    ukf.Q[3:6, 3:6] = Q_discrete_white_noise(3, dt=1, var=2)

    # REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
    # I started with the same code and the ukf.py file and made changes
    filter_state = []

    # REFERENCE: Based on sample found at http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
    # ukf.batch_filter(meas[0:-1,:])
    for z in meas[:-2,:]:
        ukf.update(z)
        ukf.predict()
        # filter_state.append(ukf.x.copy())

    x = meas[-2,:]
    for i in range(n_points):
        ukf.update(x)
        ukf.predict()
        filter_state.append(ukf.x.copy())
        x = ukf.x[0:4]

    filter_state = np.array(filter_state)

    return filter_state[:,0:2]

# main_meas = []
# for i in range(1,50):
#     main_meas.append([i, i, 1, math.pi/4.])
# main_meas = np.array(main_meas)
# # print main_meas

def test_it():
    main_meas = m.get_measurements('inputs/test01.txt')
    main_f_state = run_ukf(main_meas[:,0:4], 60)

    # # REFERENCE: https://stackoverflow.com/questions/1349230/matplotlib-coord-sys-origin-to-top-left
    plt.gca().invert_yaxis()

    plt.minorticks_on()

    main_meas = np.array(main_meas)
    plt.plot(main_meas[-5:-1,0], main_meas[-5:-1,1], 'rx')
    plt.plot(main_f_state[:, 0], main_f_state[:, 1], 'go')

    plt.show()

test_it()
