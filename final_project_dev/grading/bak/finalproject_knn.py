# You want to make sure your version produces better error rates than this :)

import sys
import measurement as m
import position as pos
import math
import numpy as np
# import sklearn.neighbors as sk

import knn as ml

filename = sys.argv[1]
# x, y = open(filename, 'r').readlines()[-1].split(',')
# train_data = m.get_measurements('inputs/train_data_full_tmp.txt')
meas = m.get_measurements(filename)

knn = ml.knn(meas)
output = knn.get_predictions(meas[-1,0:2], meas[-2,2:4], 60)


with open('prediction.txt', 'w') as f:
    for i in range(60):
        o = output[i]
        # print o
        print >> f, '{},{}'.format(int(o[0]), int(o[1]))
