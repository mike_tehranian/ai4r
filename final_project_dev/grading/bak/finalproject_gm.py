# You want to make sure your version produces better error rates than this :)

import sys
import measurement as m
import position as pos
import math
import numpy as np
import crash_library as cl
# import sklearn.neighbors as sk

import knn as ml

# REFERENCE: copied this from the robot.py class provided with project 2
# and made some modifications
def angle_trunc(t):
    return ((t+2.*math.pi) % (2.*math.pi))

filename = sys.argv[1]
# x, y = open(filename, 'r').readlines()[-1].split(',')
# train_data = m.get_measurements('inputs/train_data_full_tmp.txt')
meas = m.get_measurements(filename)

velocity_mean = meas[0,2]
velocity_variance = 100.
measurement_variance = 100.

bearing_mean = meas[0,3]
bearing_variance = 100.
measurement_variance = 100.

# print 'mean_velocity, mean_bearing', velocity_mean, bearing_mean
counter = 0

for i in range(len(meas)):
    m = meas[i,:]
    if i > 5:
        if cl.check_crashing(meas[i-5:i+1,0:2]) != None:
            velocity_variance = 100.
            bearing_variance = 100.
            
    point_velocity = m[2]
    velocity_mean = (point_velocity*velocity_variance + velocity_mean*measurement_variance)
    velocity_mean /= (velocity_variance + measurement_variance)
    velocity_variance = 1/((1/measurement_variance)+(1/velocity_variance))
    point_bearing = m[3]
    # bearing_mean = (point_bearing*bearing_variance + bearing_mean*measurement_variance)
    # bearing_mean /= (bearing_variance + measurement_variance)
    bearing_total_var =  measurement_variance + bearing_variance

    # REFERENCES: https://en.wikipedia.org/wiki/Mean_of_circular_quantities,
    # http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
    # & https://stackoverflow.com/questions/1686994/weighted-average-of-angles
    bearing_mean = math.atan2((bearing_variance/bearing_total_var)*math.sin(point_bearing)+(measurement_variance/bearing_total_var)*math.sin(bearing_mean),
        (bearing_variance/bearing_total_var)*math.cos(point_bearing)+(measurement_variance/bearing_total_var)*math.cos(bearing_mean))

    bearing_variance = 1/((1/measurement_variance)+(1/bearing_variance))
    # print 'mean_velocity, mean_bearing', velocity_mean, bearing_mean

# print velocity_mean, velocity_variance
# print bearing_mean, bearing_variance
# print meas[-10:,0:4]
next_point = meas[-1,0:2]
# print next_point

bearing_mean = angle_trunc(bearing_mean)
# print 'bearing mean is', bearing_mean

next_points = []
crashed = 0
with open('prediction.txt', 'w') as f:
    for i in range(60):
        next_point[0] += velocity_mean*math.cos(bearing_mean) 
        if crashed == 0:
            next_point[1] -= velocity_mean*math.sin(bearing_mean) 
        else: 
            next_point[1] += velocity_mean*math.sin(bearing_mean) 
        next_points.append(next_point.tolist())
        crash_points = []
        if len(next_points) > 5:
            crash_points = np.array(next_points)[-5:]
        else:
            missing_points = len(next_points) - 5
            crash_points = np.concatenate((meas[missing_points:,0:2],
                np.array(next_points)), axis=0)

        crash_state = cl.check_crashing(crash_points) 
        # print tmp[-5:]
        if crash_state != None:
            print crash_points
            print 'crash_state', crash_state, bearing_mean
            wait_time = int(crash_state[0])
            # bearing_mean = bearing_mean + 2.*(crash_state[1] - bearing_mean)
            bearing_mean = (bearing_mean +crash_state[1] + math.pi) % (2*math.pi) 
            print bearing_mean

            bearing_mean = angle_trunc(bearing_mean)

            next_point = np.array(next_points.pop())
            for z in range(wait_time+1):
                next_points.append(next_point)
            velocity_mean *= .75
        if velocity_mean < 8.:
            velocity_mean *= 1.01
        elif velocity_mean > 8.:
            velocity_mean *= .99

    for p in next_points:
        print >> f, '{},{}'.format(int(p[0]), int(p[1]))

    print next_points
