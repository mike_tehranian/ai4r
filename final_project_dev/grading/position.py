###############################################################
# Gets next position given the current x and y position along
# with the bearing and velocity
# function: get_next_position
# input: x, y, velocity, bearing
# output: next x and next y
###############################################################


import math

def get_next_position(x0, y0, velocity, bearing):
    x1 = x0 + velocity*math.cos(bearing)
    y1 = y0 - velocity*math.sin(bearing)
    return x1, y1