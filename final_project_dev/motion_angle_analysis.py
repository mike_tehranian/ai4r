from motion_variables import *
from motion_utils import eucledian_distance
import csv
import numpy as np
import matplotlib.pyplot as plt

#############
# LOAD DATA
#############

filename = 'training_data'
f = open('trajectory/' + filename + '_crash.csv', 'r')
reader = csv.reader(f)

data = []
for row in reader:
    # Stores relative_index, x, y, boundary, distance_crash, impact_angle, reflected_angle, velocity, post_crash_time_within_d10
    data.append([float(e) for e in row])
data = np.array(data)

###########################
# Analyse reflection angle
###########################

# Make negative angle += 2pi
data[data[:,6] < 0, 6] += 2 * np.pi

# angle fit
max_angle = (3.0/4) * np.pi / 2
angle_resolution = max_angle / 5
angles = np.arange(-max_angle, max_angle + angle_resolution, angle_resolution)

# Plot for each boundary
index = (data[:,3] != Boundary.CANDLE) & (np.pi/2 >  data[:,5]) & (data[:,5] > -np.pi/2)
# plt.plot(data[index,5], data[index,6], 'or')

angles_median_box = [None] * len(angles)
for i, angle in enumerate(angles):
    index = (data[:, 3] != Boundary.CANDLE) & \
                            ((angle + angle_resolution) > data[:, 5]) & \
                            (data[:, 5] > (angle - angle_resolution))

    interim_data = data[index, 6]
    angles_median_box[i] = np.median(interim_data)

plt.plot(angles, angles_median_box, 'ok', linewidth = 2.0)

# Plot for candle
index = (data[:,3] == Boundary.CANDLE) & (np.pi/2 >  data[:,5]) & (data[:,5] > -np.pi/2)
# plt.plot(data[index,5], data[index,6], 'ob')

angles_median_candle = [None] * len(angles)
for i, angle in enumerate(angles):
    index = (data[:, 3] == Boundary.CANDLE) & \
                            ((angle + angle_resolution ) > data[:, 5]) & \
                            (data[:, 5] > (angle - angle_resolution ))

    interim_data = data[index,6]
    if angle > 1:
        interim_data[interim_data > 6] -= 2 * np.pi

    angles_median_candle[i] = np.median(interim_data)

plt.plot(angles, angles_median_candle, 'ob', linewidth = 2.0)
plt.grid(True)
plt.legend(['box','candle'])
plt.title('Reflection angle as a function of incident angle')
plt.savefig('motion_statistics/reflection_angle.png')
plt.cla()

#############
# Distance
#############

distances_median = [None] * len(angles)
for i, angle in enumerate(angles):
    index_candle = (data[:, 3] == Boundary.CANDLE) & \
                            ((angle + angle_resolution ) > data[:, 5]) & \
                            (data[:, 5] > (angle - angle_resolution ))

    index_box = (data[:, 3] != Boundary.CANDLE) & \
                   ((angle + angle_resolution) > data[:, 5]) & \
                   (data[:, 5] > (angle - angle_resolution))


    distances_median[i] = np.median(np.concatenate((data[index_box,4], data[index_candle,4] - candle_r)))

index = (data[:,3] != Boundary.CANDLE)
plt.plot(data[index,5], data[index,4], 'ob')
index = (data[:,3] == Boundary.CANDLE)
plt.plot(data[index,5], data[index,4] - candle_r, 'ob')

plt.plot(angles, distances_median, 'k', linewidth = 2.0)
plt.title('Crash distance as a function of incident angle')
plt.grid(True)
plt.savefig('motion_statistics/crash_distance.png')
plt.cla()

#############
# Crash time
#############

angles_crash_time = [None] * len(angles)
for i, angle in enumerate(angles):
    index = ((angle + angle_resolution) > data[:, 5]) & \
                (data[:, 5] > (angle - angle_resolution))

    angles_crash_time[i] = np.median(data[index,8])

plt.plot(data[:,5], data[:,8], 'ob')
plt.plot(angles, angles_crash_time, 'k', linewidth = 3.0)
plt.ylim(0,20)
plt.title('Crash duration as a function of incident angle')
plt.grid(True)
plt.savefig('motion_statistics/crash_duration_per_angle.png')
plt.cla()


x_res = (box_x[1]-box_x[0]) / 20
x_pos = np.arange(box_x[0], box_x[1] + x_res, x_res)
x_pos_crash_time = [None] * len(x_pos)
for i, x in enumerate(x_pos):
    index = ((x + x_res / 2) > data[:, 1]) & \
                (data[:, 1] > (x - x_res / 2))

    x_pos_crash_time[i] = np.median(data[index,8])

plt.plot(data[:,1], data[:,8], 'ob')
plt.plot(x_pos, x_pos_crash_time, 'k', linewidth = 3.0)
# plt.ylim(0,20)
plt.title('Crash duration as a function of x position')
plt.grid(True)
plt.savefig('motion_statistics/crash_duration_per_x_position.png')
plt.cla()

#######################
# Crash time using knn
#######################

# Key variables
knn_wait_time = [[int(e[1]), int(e[2]), int(e[8])] for e in data]
knn_wait_time = np.array(knn_wait_time)

def knn_search(xy, knn_wait_time):
    knn_distance = [None] * knn_wait_time.shape[0]
    for i in range(knn_wait_time.shape[0]):
        knn_distance[i] = eucledian_distance(xy, knn_wait_time[i,0:2])
    knn_distance = np.array(knn_distance)

    index = np.argsort(knn_distance)
    k = 10
    return np.median(knn_wait_time[index[0:k],2])

pos_x = [box_x[0]] * (box_y[1] - box_y[0]) + range(box_x[0], box_x[1] + 1, 1) + \
        [box_x[1]] * (box_y[1] - box_y[0]) + range(box_x[1], box_x[0] - 1, -1)

pos_y = range(box_y[0], box_y[1] + 1, 1) + [box_y[1]] * (box_x[1] - box_x[0]) + \
        range(box_y[1], box_y[0] - 1, -1) + [box_y[0]] * (box_x[1] - box_x[0])

pos = np.transpose(np.array([pos_x, pos_y]))

t_avg = []
pos_crash_time = [None] * pos.shape[0]
for i, xy in enumerate(pos):
    pos_crash_time[i] = knn_search(xy, knn_wait_time)

plt.plot(pos_crash_time, 'k')
plt.title('Crash duration based on knn')
plt.grid(True)
plt.savefig('motion_statistics/crash_duration_with_knn.png')
plt.cla()


#############
# Save data
#############

crash_data = {}
crash_data['angles'] = [float(e) for e in angles]
crash_data['max_angle'] = float(max_angle)
crash_data['angles_median_box'] = [float(e) for e in angles_median_box]
crash_data['angles_median_candle'] = [float(e) for e in angles_median_candle]
crash_data['distances_median'] = [float(e) for e in distances_median]
crash_data['knn_wait_time'] = [[int(e[1]), int(e[2]), int(e[8])] for e in data]

import pickle
pickle.dump(crash_data, open( "crash_data.p", "wb" ))

crash_data = pickle.load( open( "crash_data.p", "rb" ) )