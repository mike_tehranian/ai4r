import numpy as np
import math
from feature_creation import get_features, N_FRAMES
# ML approach
# Inspired by http://scikit-learn.org/stable/auto_examples/ensemble/plot_gradient_boosting_regression.html#sphx-glr-auto-examples-ensemble-plot-gradient-boosting-regression-py
from sklearn import ensemble
from sklearn.multioutput import MultiOutputRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_squared_error
from crash_library import check_crashing
from motion_variables import *
import pickle

filename = 'training_data'
f = open('inputs/' + filename + '.txt', 'r')
data = []
for line in f.readlines():
    row = line.strip().split(',')
    data.append([float(e) for e in row])
f.close()
data = np.array(data)

scale_mean = np.mean(data[:, 2:-2], axis=0)
scale_std = np.std(data[:, 2:-2], axis=0)

data[:,2] = (data[:,2] - scale_mean[0]) / scale_std[0]
data[:,3] = (data[:,3] - scale_mean[1]) / scale_std[1]
data[:,4] = (data[:,4] - scale_mean[2]) / scale_std[2]


pos = data[:, 0:2]
X = data[:, 2:-2] # [boundary_distance, last_angle, last_velocity]
y = data[:, -2:] # [next_velocity, next_added_angle]

# REFERENCE:
# Inspired by http://scikit-learn.org/stable/auto_examples/ensemble/plot_gradient_boosting_regression.html#sphx-glr-auto-examples-ensemble-plot-gradient-boosting-regression-py
clf = KNeighborsRegressor(n_neighbors = 15)
# params = {'n_estimators': 1000, 'max_depth': 3, 'min_samples_split': 2,
#           'learning_rate': 0.1, 'loss': 'ls'}
# clf = MultiOutputRegressor(ensemble.GradientBoostingRegressor(**params))
clf.fit(X, y)

pickle.dump(clf, open( "clf.p", "wb" ))

# clf = pickle.load( open( "crash_data.p", "rb" ) )