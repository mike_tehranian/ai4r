# Testing script to make sure your finalproject.py operates correctly with
# the grading environment.
#
#   Place grading.py in the directory with finalproject.py
#   Create an actual directory above the directory containing finalproject.py
#       Place any files to compare your predictions to in ..\actual
#       and name them grade01.txt, grade02.txt....
#
#   basedirectory/
#       actual/
#           grade01.txt
#           grade02.txt
#           .
#           .
#           .
#       finalproject/
#           finalproject.py
#           grading.py
#           prediction.txt
#           inputs/
#               test01.txt
#               test02.txt
#               .
#               .
#               .
#   
#   
#   Use the following (or similar) in your finalproject.py to open the input file
#   passed on the command line:
#           input_file = open(sys.argv[1], 'r')
#
#   Use the following (or similar) in your finalproject.py to open the prediction.txt file
#           output_file = open('prediction.txt','w+')
#
#   To call this grading script execute the following from a command line:
#       python grading.py  


import os, sys
import numpy as np
import matplotlib.pyplot as plt

box_x = (80, 566)
box_y = (34, 325)
candle_xy = (333, 178)
candle_r = 39.4

def plot_actual_predictions(train, actual, prediction, filename=None):

    plt.grid(True)

    # Show trajectory
    plt.plot(train[:, 0], train[:, 1], 'b')
    plt.plot(actual[:, 0], actual[:, 1], 'og')
    plt.plot(prediction[:, 0], prediction[:, 1], 'oy')

    # Add box
    plt.plot([box_x[0], box_x[0]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Left wall
    plt.plot([box_x[1], box_x[1]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Right wall
    plt.plot([box_x[0], box_x[1]], [box_y[0], box_y[0]], 'k', linewidth=3.0)  # Top wall per the video
    plt.plot([box_x[0], box_x[1]], [box_y[1], box_y[1]], 'k', linewidth=3.0)  # Bottom wall per the video

    # Add circle
    circle = plt.Circle(candle_xy, candle_r, fill=False, color='k', linewidth=3.0)
    fig = plt.gcf()
    ax = fig.gca()
    ax.add_artist(circle)


    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)

    plt.close()

NUM_INPUTS = 10

def error(l1, l2):
    return sum((c - a)**2 + (d - b)**2 for ((a, b), (c, d)) in zip(l1, l2))**0.5

def convert_line(line):
    x, y = line.split(',')
    return int(x.strip()), int(y.strip())

inputs = ["./inputs/test%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
actuals = ["../actual/%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
tests = []
tests_baseline = []
for i, filename in enumerate(inputs):
    os.system('python finalproject_gbr.py %s' % (filename))
    last_position = []
    tests.append(([convert_line(line) for line in open('prediction.txt', 'r').readlines()],
                  [convert_line(line) for line in open(actuals[i], 'r').readlines()]))
    for line in open(actuals[i], 'r').readlines():
        last_position = convert_line(line)
    # print last_position

    tests_baseline.append(([last_position] * 60, \
                [convert_line(line) for line in open(actuals[i], 'r').readlines()]))

    prediction = np.array([convert_line(line) for line in open('prediction.txt', 'r').readlines()])
    actual = np.array([convert_line(line) for line in open(actuals[i], 'r').readlines()])
    train = np.array([convert_line(line) for line in open(inputs[i], 'r').readlines()])

    plot_actual_predictions(train, actual, prediction, "cross_training_pic_%s" % (i))

errors = sorted([error(l1, l2) for (l1, l2) in tests])
# print sum(errors[1:-1]) / (NUM_INPUTS - 2)

errors_baseline = sorted([error(l1, l2) for (l1, l2) in tests_baseline])

print 'total error for prediction:', sum(errors[1:-1]) / (NUM_INPUTS - 2)

print 'baseline error:', sum(errors_baseline[1:-1]) / (NUM_INPUTS - 2)
print 'error/baseline:', (sum(errors[1:-1]) / (NUM_INPUTS - 2)) / (sum(errors_baseline[1:-1]) / (NUM_INPUTS - 2))

