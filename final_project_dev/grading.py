# Testing script to make sure your finalproject.py operates correctly with
# the grading environment.
#
#   Place grading.py in the directory with finalproject.py
#   Create an actual directory above the directory containing finalproject.py
#       Place any files to compare your predictions to in ..\actual
#       and name them grade01.txt, grade02.txt....
#
#   basedirectory/
#       actual/
#           grade01.txt
#           grade02.txt
#           .
#           .
#           .
#       finalproject/
#           finalproject.py
#           grading.py
#           prediction.txt
#           inputs/
#               test01.txt
#               test02.txt
#               .
#               .
#               .
#   
#   
#   Use the following (or similar) in your finalproject.py to open the input file
#   passed on the command line:
#           input_file = open(sys.argv[1], 'r')
#
#   Use the following (or similar) in your finalproject.py to open the prediction.txt file
#           output_file = open('prediction.txt','w+')
#
#   To call this grading script execute the following from a command line:
#       python grading.py  


import os, sys

NUM_INPUTS = 10

def error(l1, l2):
    return sum((c - a)**2 + (d - b)**2 for ((a, b), (c, d)) in zip(l1, l2))**0.5

def convert_line(line):
    x, y = line.split(',')
    return int(x.strip()), int(y.strip())

inputs = ["./inputs/test%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
actuals = ["../actual/grade%02d.txt" % i for i in range(1, NUM_INPUTS+1)]
tests = []
for i, filename in enumerate(inputs):
    os.system('python finalproject.py %s' % (filename))
    tests.append(([convert_line(line) for line in open('prediction.txt', 'r').readlines()],
                  [convert_line(line) for line in open(actuals[i], 'r').readlines()]))

errors = sorted([error(l1, l2) for (l1, l2) in tests])
print sum(errors[1:-1]) / (NUM_INPUTS - 2)

