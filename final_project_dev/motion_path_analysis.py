import numpy as np
import math
import csv
import matplotlib.pyplot as plt

from motion_utils import eucledian_distance

##################################################################
# Estimates path characteristics based on data & crashes         #
##################################################################

filename = 'test01'

# Load data
f = open('inputs/' + filename + '.txt', 'r')
data = []
for line in f.readlines():
    x, y = line.strip().split(',')
    data.append((int(x), int(y)))
data = np.array(data)
f.close()

f = open('trajectory/' + filename + '_crash.csv', 'r')
reader = csv.reader(f)
crashes = []
for row in reader:
    crashes.append(int(row[0]))
f.close()

# Analysis start
for i in range(len(crashes) - 1):
    start = crashes[i]
    end = crashes[i + 1]

    distances = []
    for ii in range(start, end):
        distances.append(eucledian_distance(data[ii, :], data[ii + 1, :]))

    path_len = end-start
    coefs = np.polyfit(range(path_len), distances, 2)
    distances_fitted = np.polyval(coefs, range(path_len))

    if True: # Plot fitted data by bins
        if path_len < 15:
            plt.subplot(3, 1, 1)
            plt.plot(distances_fitted, 'b')
            plt.grid(True)
            plt.xlim(0, 100)
            plt.ylim(0, 10)

        elif path_len < 30:
            plt.subplot(3, 1, 2)
            plt.plot(distances_fitted, 'b')
            plt.grid(True)
            plt.xlim(0, 100)
            plt.ylim(0, 10)

        else:
            plt.subplot(3, 1, 3)
            plt.plot(distances_fitted, 'b')
            plt.grid(True)
            plt.xlim(0, 100)
            plt.ylim(0, 10)

    else: # Plot each data fit individually
        plt.plot(distances, 'b')
        plt.plot(distances_fitted, 'r')
        plt.savefig('trajectory/test_fit' + str(path_len) + '_' + str(i) + '.png')
        plt.close()

plt.show()