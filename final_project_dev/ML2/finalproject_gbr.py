# REFERENCES:
#
# Used in previous revisions of solution
# * Thrun, Sebastian. Udacity Lecture #4 (Kalman Filters)
# * Labbe, Roger. Kalman and Bayesian Filters in Python - http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/table_of_contents.ipynb
# * Numpy and Scipy docs - https://docs.scipy.org/doc/
# * Python documentation - https://docs.python.org/2/index.html
# * Wikipedia article on averaging angles - https://en.wikipedia.org/wiki/Mean_of_circular_quantities,
# * Roger Labbe's paragraph on averaging angles - http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
# * StackOverflow post on weighted averages of angles - https://stackoverflow.com/questions/1686994/weighted-average-of-angles
#
# Used in current revision of solution
# * Scikit Documentation - http://scikit-learn.org/stable/documentation.html
# * http://scikit-learn.org/stable/auto_examples/ensemble/plot_gradient_boosting_regression.html#sphx-glr-auto-examples-ensemble-plot-gradient-boosting-regression-py

import numpy as np
import math
from feature_creation import get_features, N_FRAMES
from sklearn.neighbors import KNeighborsRegressor
from crash_library import check_crashing
from motion_variables import *
import pickle
import sys

# Get knn regression
knn_regression = pickle.load(open( "knn_regression.p", "rb" ))
clf = knn_regression['clf']
scale_mean = knn_regression['scale_mean']
scale_std = knn_regression['scale_std']


# REFERENCE: Used np.loadtxt based on post located at
#  https://stackoverflow.com/questions/11023411/how-to-import-csv-data-file-into-scikit-learn
filename = sys.argv[1]
input_data = np.loadtxt(filename, delimiter=',') # load file

known = input_data

end = known.shape[0] + 60 -1

for i in range(known.shape[0] - 1, known.shape[0] + 60 -1, 1):
    # print i
    boundary_distance, last_angle, last_velocity, last_bearing, last_debug_points = get_features(known, i, N_FRAMES)

    boundary_distance = (boundary_distance - scale_mean[0]) / scale_std[0]
    last_angle = (last_angle - scale_mean[1]) / scale_std[1]
    last_velocity = (last_velocity - scale_mean[2]) / scale_std[2]

    new_predict = clf.predict([[boundary_distance, last_angle, last_velocity]])[0]

    new_x = known[i, 0] + new_predict[0] * math.cos(last_bearing + new_predict[1])
    new_y = known[i, 1] + new_predict[0] * math.sin(last_bearing + new_predict[1])

    if new_x < box_x[0] or new_x > box_x[1] or new_y < box_y[0] or new_y > box_y[1]:
        while i < end:
            known = np.append(known, [known[i, :]], axis=0)
            i +=1
        break
    else:
        known = np.append(known, [[new_x, new_y]], axis=0)

predicted_points = known[-61:-1, :]
# print predicted_points.shape
# print known

with open('prediction.txt', 'w') as f:
    for i in range(len(predicted_points)):
        # print predicted_points
        print >> f, '{},{}'.format(int(predicted_points[i][0]), int(predicted_points[i][1]))

