from crash_library import check_crashing, LEN_REQ
import numpy as np
import matplotlib.pyplot as plt
from motion_variables import *

# Plot trajectory, and bounding boxes
def plot_data_with_boundaries(data, trajectory, filename, title):

    plt.grid(True)
    plt.title(title)
    # Add box
    plt.plot([box_x[0], box_x[0]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Left wall
    plt.plot([box_x[1], box_x[1]], [box_y[0], box_y[1]], 'k', linewidth=3.0)  # Right wall
    plt.plot([box_x[0], box_x[1]], [box_y[0], box_y[0]], 'k', linewidth=3.0)  # Top wall per the video
    plt.plot([box_x[0], box_x[1]], [box_y[1], box_y[1]], 'k', linewidth=3.0)  # Bottom wall per the video

    # Show trajectory
    plt.plot(data[:, 0], data[:, 1], 'b')
    plt.plot(trajectory[0], trajectory[1], 'or')

    # Add circle
    circle = plt.Circle(candle_xy, candle_r, fill=False, color='k', linewidth=3.0)
    fig = plt.gcf()
    ax = fig.gca()
    ax.add_artist(circle)


    if filename is None:
        plt.show()
    else:
        plt.savefig('trajectory/' + filename)

    plt.close()

# Load data
filename = 'test01'
f = open('inputs/' + filename + '.txt', 'r')
data = []
for line in f.readlines():
    x, y = line.strip().split(',')
    data.append((int(x), int(y)))
data = np.array(data)
f.close()

# Test crash_library
for i in range(LEN_REQ, data.shape[0] + 1, 1):
    result = check_crashing(data[i - LEN_REQ:i, :])
    if result is not None:
        plot_data_with_boundaries(data[i - 30:i + 30, :],
                                  data[i,:],
                                  filename="crash_%s" % i,
                                  title = "wait: %s, angle: %s" % (result[0], result[1]))