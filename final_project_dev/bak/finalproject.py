# You want to make sure your version produces better error rates than this :)

import sys
import measurement as m
import position as pos
import math
import numpy as np

filename = sys.argv[1]
# x, y = open(filename, 'r').readlines()[-1].split(',')
meas = m.get_measurements(filename)
# print meas[-2,:]
velocity = meas[-2,2]
bearing = meas[-2,3]

# print velocity, bearing

output = []
print meas
x0, y0 = meas[-2,0], meas[-2,1]

for i in range(0, 60):
    x1, y1 = pos.get_next_position(x0, y0, velocity, bearing)
    # x1 = x0 + velocity*math.cos(bearing)
    # y1 = y0 - velocity*math.sin(bearing)
    print x0, y0, x1, y1
    output.append([x1, y1])
    x0, y0 = x1, y1

with open('prediction.txt', 'w') as f:
    for i in range(60):
        o = output[i]
        # print o
        print >> f, '{},{}'.format(int(o[0]), int(o[1]))

# REFERENCE: https://stackoverflow.com/questions/9777783/suppress-scientific-notation-in-numpy-when-creating-array-from-nested-list
np.set_printoptions(suppress=True)

with open('measurements.txt', 'w') as f:
    for i in range(len(meas)):
        o = np.round(meas[i,:],3)
        # print o
        print >> f, '{}'.format(o)


# np.savetxt('measurements.txt', meas, delimiter=',')