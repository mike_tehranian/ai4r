# Example of UKF

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import math
import random
import numpy as np

from filterpy.kalman import UnscentedKalmanFilter as UKF


def hx(x):
    """ the measurement function converts the filter's prior state x into a
    measurement vector of shape (dim_z). """
    return x


def fx(state, dt):
    """ state transition function for state [x, y, heading, turning, distance].
    dt is the time step in seconds. """
    x, y, heading, turning, distance = [state.value[i][0] for i in range(state.dimx)]

    new_heading = angle_trunc(heading + turning)

    # Nonlinear State Equations:
    x = x + distance * cos(new_heading)
    y = y + distance * sin(new_heading)
    heading = new_heading
    turning = turning
    distance = distance

    return np.array([x, y, heading, turning, distance])


def Q_model_uncertainty(dt=1., var=1.):
    """ Returns the Q matrix for the Discrete Constant White Noise
    Model. dim is 5, dt is the time step, and sigma is the
    variance in the noise.
    Q is computed as the G * G^T * variance, where G is the process noise per
    time step. In other words, G = [[.5dt^2][dt]]^T for the constant velocity
    model.
    Parameters
    -----------
    dt : float, default=1.0
        time step in whatever units your filter is using for time. i.e. the
        amount of time between innovations
    var : float, default=1.0
        variance in the noise
    """
    Q = np.array([[0.05*dt, 0., 0., 0., 0.],
                 [0., 0.05*dt, 0., 0., 0.],
                 [0., 0., 0.01*dt, 0., 0.],
                 [0., 0., 0., 0.01*dt, 0.],
                 [0., 0., 0., 0., 0.05*dt]], dtype=float)
    return Q * var


def R_measurement_noise(dt=1., var=1.):
    """
    Parameters
    -----------
    dt : float, default=1.0
        time step in whatever units your filter is using for time. i.e. the
        amount of time between innovations
    var : float, default=1.0
        variance in the noise
    """
    R = np.array([[1.*dt, 0., 0., 0., 0.],
                 [0., 1.*dt, 0., 0., 0.],
                 [0., 0., math.pi*dt, 0., 0.],
                 [0., 0., 0., math.pi*dt, 0.],
                 [0., 0., 0., 0., 1.*dt]], dtype=float)
    return R * var


if __name__ == "__main__":
    dt = 0.05

    # β=2β=2 is a good choice for Gaussian problems, κ=3−nκ=3−n where nn is the dimension of xx is a good choice for κκ, and 0≤α≤10≤α≤1 is an appropriate choice for αα, where a larger value for αα spreads the sigma points further from the mean.
    # points = MerweScaledSigmaPoints(n=2, alpha=.3, beta=2., kappa=.1)
    # points = MerweScaledSigmaPoints(n=4, alpha=.1, beta=2., kappa=-1.)
    points = MerweScaledSigmaPoints(n=4, alpha=.1, beta=2., kappa=1.)
    ukf = UKF(dim_x=5, dim_z=5, dt=dt, hx=hx, fx=fx, points=points)
    # initial state: x, y, heading, turning, and distance
    ukf.x = np.array([0., 0., 0., 0., 0.])
    ukf.P = np.diag([100., 100., math.pi, math.pi, 100.])
    ukf.R = np.diag([1., 1., math.pi, math.pi, 1.])
    ukf.Q *= Q_model_uncertainty(dt, .01)

    filter_state = []
    for z in measurements:
        ukf.predict()
        ukf.update(z)
        filter_state.append(ukf.x.copy())
    filter_state = np.array(filter_state)

    plt.plot(filter_state[:, 0], filter_state[:, 1])
    print('UKF standard deviation {:.3f} meters'.format(np.std(filter_state - measurements)))
