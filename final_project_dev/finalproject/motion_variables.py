# Positions based on motion_boundary.py
box_x = (80, 566)
box_y = (34, 325)
candle_xy = (333, 178)
candle_r = 39.4

class Boundary:
    X_LEFT = 1
    X_RIGHT = 2
    Y_TOP = 3
    Y_BOTTOM = 4
    CANDLE = 5