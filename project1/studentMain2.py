# ----------
# Part Two
#
# Now we'll make the scenario a bit more realistic. Now Traxbot's
# sensor measurements are a bit noisy (though its motions are still
# completetly noise-free and it still moves in an almost-circle).
# You'll have to write a function that takes as input the next
# noisy (x, y) sensor measurement and outputs the best guess
# for the robot's next position.
#
# ----------
# YOUR JOB
#
# Complete the function estimate_next_pos. You will be considered
# correct if your estimate is within 0.01 stepsizes of Traxbot's next
# true position.
#
# ----------
# GRADING
#
# We will make repeated calls to your estimate_next_pos function. After
# each call, we will compare your estimated position to the robot's true
# position. As soon as you are within 0.01 stepsizes of the true position,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot.

# These import steps give you access to libraries which you may (or may
# not) want to use.
from robot import *  # Check the robot.py tab to see how this works.
from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random

# measurement function: reflect the fact that we observe x, y, heading,
# turning
H = matrix([[1., 0., 0., 0.],
            [0., 1., 0., 0.],
            [0., 0., 1., 0.],
            [0., 0., 0., 1.]])
# measurement uncertainty
R = matrix([[1., 0., 0., 0.],
            [0., 1., 0., 0.],
            [0., 0., pi, 0.],
            [0., 0., 0., pi/10]])
# 4D identity matrix
I = matrix([[1., 0., 0., 0.],
            [0., 1., 0., 0.],
            [0., 0., 1., 0.],
            [0., 0., 0., 1.]])

# This is the function you have to write. Note that measurement is a
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    if OTHER is None:
        OTHER = {}
        OTHER['prev_measurement'] = [measurement]
        # initial state: x, y, heading, and turning
        OTHER['state'] = matrix([[0.], [0.], [0.], [0.]])
        # initial uncertainty: 100 for positions x, y, heading, and turning
        OTHER['covariance'] = matrix([
                    [100., 0., 0., 0.],
                    [0., 100., 0., 0.],
                    [0., 0., pi, 0.],
                    [0., 0., 0., pi]])
        return measurement, OTHER

    OTHER['prev_measurement'].append(measurement)

    if len(OTHER['prev_measurement']) < 3:
        return measurement, OTHER

    x0, y0 = OTHER['prev_measurement'][-3]
    x1, y1 = OTHER['prev_measurement'][-2]
    x2, y2 = OTHER['prev_measurement'][-1]
    heading_0 = atan2(y1-y0, x1-x0)
    heading_1 = atan2(y2-y1, x2-x1)

    turning = heading_1 - heading_0
    if turning < -pi:
        # Check to see if heading went from +pi to -pi
        turning = 2*pi + turning
    elif turning > pi:
        # Check to see if heading went from -pi to +pi
        turning = 2*pi - turning

    state = OTHER['state']
    covariance = OTHER['covariance']
    distance = distance_between((x2, y2), (x1, y1))

    # model uncertainty
    Q = matrix([[0.05, 0., 0., 0.],
                [0., 0.05, 0., 0.],
                [0., 0., 0.01, 0.],
                [0., 0., 0., 0.01]])

    # Create the Z vector
    new_measurement = (x2, y2, heading_1, turning)

    new_state, new_covariance = extended_kalman_filter(state,
            covariance,
            new_measurement,
            distance,
            H,
            R,
            I,
            Q)

    OTHER['state'] = new_state
    OTHER['covariance'] = new_covariance

    xy_estimate = (new_state.value[0][0], new_state.value[1][0])
    return xy_estimate, OTHER

def extended_kalman_filter(x, P, new_measurement, distance, H, R, I, Q):
    # Update
    # Measurement function H is linear
    Z = matrix([new_measurement])
    # error: very important term
    y = Z.transpose() - (H * x)
    # residual covariance
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)
    P = (I - (K * H)) * P

    # Predict
    # Use non-linear g(x) and the Jacobian G
    x_predict = g(x, distance)
    G = calculate_jacobian(x_predict, distance)
    P_predict = G * P * G.transpose() + Q

    return x_predict, P_predict

def g(state, distance):
    x, y, heading, turning = [state.value[i][0] for i in range(state.dimx)]

    new_heading = angle_trunc(heading + turning)

    # Nonlinear State Equations:
    x = x + distance * cos(new_heading)
    y = y + distance * sin(new_heading)
    heading = new_heading
    turning = turning

    return matrix([[x], [y], [heading], [turning]])

def calculate_jacobian(state, distance):
    x, y, heading, turning = [state.value[i][0] for i in range(state.dimx)]

    # State equations:
    # x_t = x_t-1 + distance*cos(heading + turning)
    # y_t = y_t-1 + distance*sin(heading + turning)
    # heading_t = heading_t-1 + turning
    # turning_1 = turning_t-1
    dx_dtheta = -distance * sin(heading + turning)
    dy_dtheta = distance * cos(heading + turning)

    # Jacobian of the State Equations
    G = matrix([[1., 0., dx_dtheta, dx_dtheta],
                [0., 1., dy_dtheta, dy_dtheta],
                [0., 0., 1., 1.],
                [0., 0., 0., 1.]])
    return G

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# This is here to give you a sense for how we will be running and grading
# your code. Note that the OTHER variable allows you to store any
# information that you want.
def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    while not localized and ctr <= 1000:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        if error <= distance_tolerance:
            print "You got it right! It took you ", ctr, " steps to localize."
            localized = True
        if ctr == 1000:
            print "Sorry, it took you too many steps to localize the target."
    return localized

# This is a demo for what a strategy could look like. This one isn't very good.
def naive_next_pos(measurement, OTHER = None):
    """This strategy records the first reported position of the target and
    assumes that eventually the target bot will eventually return to that
    position, so it always guesses that the first position will be the next."""
    if not OTHER: # this is the first measurement
        OTHER = measurement
    xy_estimate = OTHER
    return xy_estimate, OTHER

def demo_grading_visual(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.02 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    #For Visualization
    import turtle    #You need to run this locally to use the turtle module
    window = turtle.Screen()
    window.bgcolor('white')
    size_multiplier= 25.0  #change Size of animation
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.1, 0.1, 0.1)
    measured_broken_robot = turtle.Turtle()
    measured_broken_robot.shape('circle')
    measured_broken_robot.color('red')
    measured_broken_robot.resizemode('user')
    measured_broken_robot.shapesize(0.1, 0.1, 0.1)
    prediction = turtle.Turtle()
    prediction.shape('arrow')
    prediction.color('blue')
    prediction.resizemode('user')
    prediction.shapesize(0.1, 0.1, 0.1)
    prediction.penup()
    broken_robot.penup()
    measured_broken_robot.penup()
    #End of Visualization
    while not localized and ctr <= 1000:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        if error <= distance_tolerance:
            print "You got it right! It took you ", ctr, " steps to localize."
            localized = True
            import time
            time.sleep(5)
        if ctr == 1000:
            print "Sorry, it took you too many steps to localize the target."
            import time
            time.sleep(5)
        #More Visualization
        measured_broken_robot.setheading(target_bot.heading*180/pi)
        measured_broken_robot.goto(measurement[0]*size_multiplier, measurement[1]*size_multiplier-200)
        measured_broken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-200)
        broken_robot.stamp()
        prediction.setheading(target_bot.heading*180/pi)
        prediction.goto(position_guess[0]*size_multiplier, position_guess[1]*size_multiplier-200)
        prediction.stamp()
        #End of Visualization
    return localized

