import numpy as np
from sklearn.neighbors import KNeighborsRegressor


def get_knn_regressor():
    # Load training data
    filename = 'training_data'
    f = open('features/' + filename + '.txt', 'r')
    data = []
    for line in f.readlines():
        row = line.strip().split(',')
        data.append([float(e) for e in row])
    f.close()
    data = np.array(data)

    # Load test files for additional training data if needed
    for i in range(1, 11, 1):
        filename = 'test%02d' % i

        f = open('features/' + filename + '.txt', 'r')
        tmp_data = []
        for line in f.readlines():
            row = line.strip().split(',')
            tmp_data.append([float(e) for e in row])
        f.close()
        tmp_data = np.array(tmp_data)

        data = np.append(data, tmp_data, axis=0)

    # Get center and scaling
    scale_mean = np.mean(data[:, 2:-2], axis=0)
    scale_std = np.std(data[:, 2:-2], axis=0)

    data[:, 2] = (data[:, 2] - scale_mean[0]) / scale_std[0]
    data[:, 3] = (data[:, 3] - scale_mean[1]) / scale_std[1]
    data[:, 4] = (data[:, 4] - scale_mean[2]) / scale_std[2]

    # Fit
    pos = data[:, 0:2]
    X = data[:, 2:-2]  # [boundary_distance, last_angle, last_velocity]
    y = data[:, -2:]  # [next_velocity, next_added_angle]

    # REFERENCE: http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsRegressor.html
    clf = KNeighborsRegressor(n_neighbors=15)
    clf.fit(X, y)

    knn_regression = {}
    knn_regression['clf'] = clf
    knn_regression['scale_mean'] = scale_mean
    knn_regression['scale_std'] = scale_std

    return knn_regression