Pre-requisites:
* Scikit Learn (http://scikit-learn.org/stable/index.html) -- This is already installed by default on the VM
* features -- This folder contains the files used to train the knn classifier

Description of Files & Folders
- Files to create precomputed values
* feature_creation.py -- This file takes all of the files within features_input and finds the velocity, bearing, angular velocity and distance to the closest feature for each measurement. The results are saved to features_input.
* features -- This folder contains the files used to train the knn classifier
* features_input -- This folder contains the input files used to create the training files within the features folder


- Files used during grading
* inputs -- This folder contains the input files that passed to finalproject.py to predict the next 60 points
* finalproject.py -- This file processes in the input files and uses a KNN classifier to predict the next 60 points
* grading.py -- This is the grading script that was provided with the project
* knn_regression.py -- This is our knn classifier. It uses the Scikit learn KNN regressor(http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsRegressor.html) and is trained using the training files in the features folder.
* motion_variables -- This file contains variables related to our motion model (bounding box and candle location)

- Other files
* members.txt -- This file lists the names of our team members.


Used in current revision of solution
* Scikit Documentation - http://scikit-learn.org/stable/documentation.html
* Scikit KNN Documentation - http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsRegressor.html


Steps to Generate the Training Files
* Go to the finalproject folder
* Run `python feature_creation.py` -- this creates the set of training files under the features folder that are used to train the KNN regressor


Approach Taken
Our team initially tried to explore using a UKF and KNN in parallel. We had used the filterpy library [https://filterpy.readthedocs.io/en/latest/#] for the UKF and encountered poor results in terms of error and performance, so we tried building a simple Kalman Filter.

The Kalman Filter code did the following:
* Called a measurements function that loaded all of the measurements into a numpy array with the velocity and bearing associated with each measurement
* Set an initial variance for the velocity and bearing based on the variance velocity and bearing variance calculated across all points
* Goes through each measurement and add the velocity and bearing to the mean velocity and bearing according to the following calculation:
    velocity_mean = (point_velocity*velocity_variance + velocity_mean*velocity_measurement_variance)
    velocity_mean /= (velocity_variance + velocity_measurement_variance)
    velocity_variance = 1/((1/velocity_measurement_variance)+(1/velocity_variance))
    bearing_total_var =  bearing_measurement_variance + bearing_variance
    # REFERENCES: https://en.wikipedia.org/wiki/Mean_of_circular_quantities,
    # http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
    # & https://stackoverflow.com/questions/1686994/weighted-average-of-angles
    bearing_mean = math.atan2((bearing_variance/bearing_total_var)*math.sin(point_bearing)+(bearing_measurement_variance/bearing_total_var)*math.sin(bearing_mean),
        (bearing_variance/bearing_total_var)*math.cos(point_bearing)+(bearing_measurement_variance/bearing_total_var)*math.cos(bearing_mean))
    bearing_variance = 1/((1/bearing_measurement_variance)+(1/bearing_variance))
* The program then took the mean velocity and bearing and plotted a straight line. It varied the acceleration slightly the keep the velocity around 8 units. It called another crashing library written by a team member that uses machine learning to detect crashes and send the best bearing angle based on the incoming crash direction. This approach resulted in error that was approximately 13% below baseline when run against 1000 samples taken from the training data.

We ultimately decided to not take this approach because one of our team members came up with a KNN approach that provided a 30% improvement over the baseline.

The KNN approach works as follows:
* Take the training data and inputs and calculate the following features: velocity, distance to closest boundary, angle with respect to closest boundary, next velocity and next added angle
   - These features are calculated over the previous and following 10 frames, to average the data.
* Train a KNN classifier to predict the next added angle and velocity based on the other features. The Scikit KNN regressor was used for this (see http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsRegressor.html)
* When passed the input, finalproject.py calls a helper function to generate the features for a given point and passes this to the KNN regressor. The KNN regressor predicts the next velocity and added angle for the given point, which is used to find the next point. The same process occurs iteratively 60 times. If the regressor leads the hexbug outside the bounds, then we stay at the same point till the end.


REFERENCES:
Used in previous revisions of solution
* Thrun, Sebastian. Udacity Lecture #4 (Kalman Filters)
* Labbe, Roger. Kalman and Bayesian Filters in Python - http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/table_of_contents.ipynb
* Numpy and Scipy docs - https://docs.scipy.org/doc/
* Python documentation - https://docs.python.org/2/index.html
* Wikipedia article on averaging angles - https://en.wikipedia.org/wiki/Mean_of_circular_quantities,
* Roger Labbe's paragraph on averaging angles - http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb
* StackOverflow post on weighted averages of angles - https://stackoverflow.com/questions/1686994/weighted-average-of-angles
* FilterPy Library - https://filterpy.readthedocs.io/en/latest/#
