import numpy as np
import math

N_FRAMES = 10

box_x = (80, 566)
box_y = (34, 325)
candle_xy = (333, 178)
candle_r = 39.4

class Boundary:
    X_LEFT = 1
    X_RIGHT = 2
    Y_BOTTOM = 3
    Y_TOP = 4
    CANDLE = 5

def eucledian_distance(loc1, loc2):
    return math.sqrt((loc1[0] - loc2[0]) ** 2 + (loc1[1] - loc2[1]) ** 2)

# Help functions
def truncate_angle(t): # from robot.py of Project 2
    return ((t + np.pi) % (2 * np.pi)) - np.pi

def get_velocity_bearing(data):
    velocity = eucledian_distance(data[-1, :], data[0, :]) / (data.shape[0] - 1)

    # Get current bearing
    if abs(np.min(data[:, 1]) - np.max(data[:, 1])) > abs(np.min(data[:, 0]) - np.max(data[:, 0])):
        # Assume direction is along y axis
        coefs = np.polyfit(data[:, 1], data[:, 0], 1)
        new_y = [data[0, 1], data[-1, 1]]
        new_x = np.polyval(coefs, new_y)
        bearing = math.atan2(new_y[1] - new_y[0], new_x[1] - new_x[0])

    else:
        # Assume direction is along x axis
        coefs = np.polyfit(data[:, 0], data[:, 1], 1)
        new_x = [data[0, 0], data[-1, 0]]
        new_y = np.polyval(coefs, new_x)
        bearing = math.atan2(new_y[1] - new_y[0], new_x[1] - new_x[0])

    debug_points = np.transpose(np.array([new_x, new_y]))

    return velocity, bearing, debug_points

def get_features(data, i, fc):
    # Find closest boundary
    boundary_distances = []
    boundary_distances.append(abs(data[i, 0] - box_x[0]))
    boundary_distances.append(abs(data[i, 0] - box_x[1]))
    boundary_distances.append(abs(data[i, 1] - box_y[0]))
    boundary_distances.append(abs(data[i, 1] - box_y[1]))
    boundary_distances.append(abs(eucledian_distance(candle_xy, data[i, :]) - candle_r))

    boundary = np.argmin(boundary_distances) + 1
    boundary_distance = boundary_distances[boundary - 1]

    # Find last fc velocity
    last_velocity, last_bearing, last_debug_points = get_velocity_bearing(data[i - fc + 1: i + 1, :])

    # Offset angle based on boundary type
    if boundary == Boundary.X_LEFT:
        last_angle = truncate_angle(last_bearing - np.pi)

    elif boundary == Boundary.X_RIGHT:
        last_angle = last_bearing

    elif boundary == Boundary.Y_BOTTOM:
        last_angle = truncate_angle(last_bearing + np.pi / 2)

    elif boundary == Boundary.Y_TOP:
        last_angle = truncate_angle(last_bearing - np.pi / 2)

    elif boundary == Boundary.CANDLE:
        last_angle = truncate_angle(last_bearing - math.atan2(candle_xy[1] - data[i, 1], candle_xy[0] - data[i, 0]))

    return boundary_distance, last_angle, last_velocity, last_bearing, last_debug_points


if __name__ == '__main__':

    files = ['training_data']
    for i in range(1,11,1):
        files.append('test%02d' % i)

    for filename in files:
        # Load training data
        # filename = 'training_data'
        print "Starting feature creation for: %s" % filename
        f = open('features_input/'+ filename + '.txt', 'r')
        data = []
        for line in f.readlines():
            x, y = line.strip().split(',')
            data.append((int(x), int(y)))
        f.close()
        data = np.array(data)

        f = open('features/'+ filename + '.txt', 'w')

        for i in range(N_FRAMES, data.shape[0] - N_FRAMES, 1):

            boundary_distance, last_angle, last_velocity, last_bearing, last_debug_points = get_features(data, i, N_FRAMES)
            # Find next velocity and bearing change
            next_velocity, next_bearing, next_debug_points = get_velocity_bearing(data[i: i + N_FRAMES, :])
            next_added_angle = truncate_angle(next_bearing - last_bearing)

            # FEATURES
            # X, Y, boundary_distance,
            new_row = [data[i,0], data[i,1]] + [boundary_distance, last_angle, last_velocity] + [next_velocity, next_added_angle]

            f.write(','.join([str(e) for e in new_row]) + '\n')
        f.close()