import numpy as np
import math
from feature_creation import get_features, N_FRAMES
from knn_regressor import get_knn_regressor
from motion_variables import *
import sys

# Get knn regression
knn_regression = get_knn_regressor()
clf = knn_regression['clf']
scale_mean = knn_regression['scale_mean']
scale_std = knn_regression['scale_std']


# REFERENCE: Used np.loadtxt based on post located at
#  https://stackoverflow.com/questions/11023411/how-to-import-csv-data-file-into-scikit-learn 
# & http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsRegressor.html
filename = sys.argv[1]
input_data = np.loadtxt(filename, delimiter=',') # load file

known = input_data

end = known.shape[0] + 60 -1

for i in range(known.shape[0] - 1, known.shape[0] + 60 -1, 1):
    # print i
    boundary_distance, last_angle, last_velocity, last_bearing, last_debug_points = get_features(known, i, N_FRAMES)

    boundary_distance = (boundary_distance - scale_mean[0]) / scale_std[0]
    last_angle = (last_angle - scale_mean[1]) / scale_std[1]
    last_velocity = (last_velocity - scale_mean[2]) / scale_std[2]

    new_predict = clf.predict([[boundary_distance, last_angle, last_velocity]])[0]

    new_x = known[i, 0] + new_predict[0] * math.cos(last_bearing + new_predict[1])
    new_y = known[i, 1] + new_predict[0] * math.sin(last_bearing + new_predict[1])

    if new_x < box_x[0] or new_x > box_x[1] or new_y < box_y[0] or new_y > box_y[1]:
        while i < end:
            known = np.append(known, [known[i, :]], axis=0)
            i +=1
        break
    else:
        known = np.append(known, [[new_x, new_y]], axis=0)

predicted_points = known[-61:-1, :]

with open('prediction.txt', 'w') as f:
    for i in range(len(predicted_points)):
        # print predicted_points
        print >> f, '{},{}'.format(int(predicted_points[i][0]), int(predicted_points[i][1]))

